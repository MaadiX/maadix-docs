Apache y Nginx
==============

El servidor web predeterminado es **Apache**, pero a partir de la versión 202405 también es posible **instalar Nginx**. En este caso, Apache seguirá habilitado, pero tendrás la opción de elegir cuál de los dos servidores web prefieres utilizar para cada dominio que agregues al panel de control.

Para permitir la coexistencia de ambos servidores, se instala **HaProxy**, que escucha en los puertos 80 y 443 de la IP pública del servidor y redirige el tráfico a las siguientes IPs locales:

-    127.0.0.2 para Apache
-    127.0.0.3 para Nginx

.. figure:: img/haproxy-nginx-apache-big-blanco.png
   :alt: Nginx y Apache con Haproxy

   HaProxy.

Nginx se ha añadido para ofrecer soporte a aplicaciones como CryptPad, Mastodon o PeerTube, siendo el servidor web recomendado para ellas. Sin embargo, hemos mantenido Apache, ya que muchas personas prefieren utilizar archivos .htaccess, los cuales pueden ser editados sin necesidad de acceder por consola utilizando las cuentas de webmaster.

Para los dominios añadidos desde el panel de control, se crean los Vhosts correspondientes en las siguientes ubicaciones:

-    ``/etc/apache2/ldap-enabled`` para Apache
-    ``/etc/nginx/ldap-enabled`` para Nginx

Si necesitas aplicar cambios a los Vhosts, no sobrescribas los archivos creados por el sistema, ya que tus cambios podrían perderse en alguna actualización o edición que hagas desde el panel de control. En su lugar, copia el archivo en la carpeta correspondiente de sites-enabled, es decir:

-    ``/etc/apache2/sites-enabled`` para Apache
-    ``/etc/nginx/sites-enabled`` para Nginx

Ten en cuenta que los archivos de dominios que hayas copiado manualmente a la carpeta sites-enabled no se eliminarán si borras el dominio desde el panel de control.


Puedes consutar esta pagina si quieres saber más sobre `Haproxy <https://www.haproxy.org/>`__ 
