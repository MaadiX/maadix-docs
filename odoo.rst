Odoo
====

Odoo es una aplicación ERP muy completa que permite la gestión de contactos, 
facturas, inventario, pedidos, pagos, etc. Indicado para comercio electrónico, 
contabilidad, punto de venta, gestión de proyectos, etc. Dispone de extensiones 
gratuitas y soluciones de pago para ampliar las funcionalidades.

Requisitos del sistema
----------------------

Antes de proceder con la instalación, es aconsejable averiguar que el
servidor tenga los recursos disponibles necesarios para un buen
funcionamiento. Odoo es una aplicación que consume bastantes recursos y 
espacio en disco. Es ideal disponer de al menos 5 GB de espacio libre 
y de 500 MB de RAM.

En el panel de control, en la sección Sistema > Detalles puedes
consultar el estado de tu sistema y los recursos disponibles.

.. figure:: img/lool/panel-de-control.png
   :alt: Estadísticas

   Estadísticas del sistema.

Instalación
-----------

Antes de proceder con la instalación es necesario decidir bajo qué
dominio o subdominio se quiere alojar la aplicación. Usaremos como
ejemplo el subdominio odoo.example.com.

Tendremos que crear una entrada DNS de tipo A, que apunte a la IP del
servidor.

``odoo.example.com A IP.DE.TU.SERVIDOR``

Según el proveedor de dominio que tengas, la propagación de los DNS
puede tardar entre pocos minutos y unas horas. Una vez los DNS estén
propagados podrás proceder a la instalación desde el panel de control.

Al hacer clic en 'Odoo', desde la página 'Instalar aplicaciones', se desplegarán dos
campos, en los que tendrás que insertar los siguientes valores:

1.\ El nombre del dominio/\subdominio que quieres utilizar para instalar la aplicación. En el caso de nuestro ejemplo será *odoo.example.com*.

2.\ Una contraseña que necesitarás para poder crear bases de datos de Odoo. Sin ella no podrás empezar a utilizar la aplicación, así que guárdala en un lugar seguro.


.. figure:: img/odoo/install.png
   :alt: Install Odoo

   Instalación de Odoo

La instalación de Odoo requiere un tiempo más largo que otras aplicaciones disponibles, así que ten paciencia hasta que termine el proceso, que puede llegar a durar hasta 10 minutos.  


Activar instalación
-------------------

Una vez terminado el proceso de instalación de Odoo recibirás un correo 
con instrucciones para acceder a la aplicación.  

La primera vez que accedas verá un formulario como el que se muestra en 
la siguiente imágen:  


.. figure:: img/odoo/activate.png
   :alt: Screenshot

   Activación de Odoo

Este paso permite crear una base de datos y una cuenta de administración 
de la aplicación.
Los datos que insertes en los campos *Email* y *Password* serán los  que 
utilizarás para acceder a la aplicación.

En el campo *Database Name* tendrás que introducir el nombre que quieras 
asignar a la base de datos que se creará al enviar el formulario.  

En el primer campo *Master Passowrd* tendrás que insertar la contraseña que 
elegiste en el momento de instalación de la aplicación desde el panel de control.  
Si no la recuerdas la puedes encontrar el archivo de configuración de la 
aplicación :

``/var/www/odoo/odoo/odoo.conf``

Para poder leer el contenido de este archivo tendrás que acceder por SSH 
con la cuenta Superusuarix. Accediendo por SFTP no podrás leer su contenido 
ya que necesitas escalar a superusuarix con el comando ``sudo su``.  

En este enlace puedes consultar la  documentación oficial de Odoo:  

https://www.odoo.com/documentation
