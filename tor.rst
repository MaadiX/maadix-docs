Tor
===
MaadiX ahora permite instalar Tor y activar servicios onion para tus dominios, usando el dominio especial .onion, accesible exclusivamente desde la red Tor.


Instalación
-----------

Encontrarás la opción de instalación en la página “Instalar Aplicaciones” del menú izquierdo del panel de control. No necesitas configurar parámetros adicionales.
Tras la instalación, al agregar o editar un dominio, podrás activar el servicio onion asociado.

Servicios Onion
---------------

Activar
~~~~~~~

Puedes habilitar servicios onion para dominios existentes o nuevos. Si necesitas un servicio sin un dominio indexado (.net, .org, etc.), puedes ingresar en el campo 'Nombre de dominio' un identificador con formato válido, como myonion.com, y desactivar el servidor web para evitar la verificación DNS.


.. figure:: img/tor/only-onion.png
   :alt: Solo dominio onion
   :align: center
   :figwidth: 60%


   Configuración para dominio ,onion 


El servicio aparerecrá en el listado de dominios como myonion.com y además se crea la carpeta correspondiente en ``/var/www/html/myonion.com`` para alojar tu aplicación.

También es posible usar servicios onion junto a dominios tradicionales indexados en la clearnet.

.. figure:: img/tor/cleranet-also-domain.png
   :alt: dominio y  onion
   :align: center
   :figwidth: 60%
   
   Configuración para dominio .onion y .com

Una vez activado el servicio onion, puedes consultar su dirección accediendo a la página de edición del dominio

.. figure:: img/tor/onion-enabled.png
   :alt: dirección onion
   :align: center
   :figwidth: 60%

   Consultar dirección .onion 


Desactivar
~~~~~~~~~~


Desactivar la aplicación  Tor o deshabilitar un servicio onion desde la página de edición del dominio conserva la configuración para reactivarlo.

Sin embrago, si desinstalas Tor o eliminas un dominio, también se elimina su configuración para Tor y se traslada a la papelera la carpeta ``/var/lib/tor/hiddenservices/mydomain.com`` cuyo contenido se describe a continuación.

Claves
~~~~~~

Por cada servicio onion se crea una carpeta en ``/var/lib/tor/hiddenservices`` con el siguiente contenido:

**hostname**
Contiene la cadena exacta de la dirección .onion

**hs_ed25519_public_key y hs_ed25519_secret_key**
Regeneran el dominio si necesitas migrarlo o recuperarlo. Se recomienda copiarlas manualmente si no tienes activadas las copias de seguridad.

**authorized_clients**
Puedes habilitar autenticación para accesos privados mediante el archivo authorized_clients. Esta configuración no está disponible desde el panel de control, pero puedes seguir las instrucciones oficiales de Tor: 

    https://community.torproject.org/onion-services/advanced/client-auth/


Wordpress
~~~~~~~~~

WordPress no puede instalarse automáticamente si el servidor web no está activado, ya que requiere un dominio válido que apunte a la IP del servidor.

Si deseas configurar un WordPress accesible únicamente mediante un dominio .onion en la red Tor, deberás realizar la instalación de forma manual. Esto implica configurar WordPress sin un dominio público, asegurándote de que todo funcione correctamente en un entorno basado únicamente en Tor.

Aquí encuentras recomendaciones útiles al respecto:

    https://riseup.net/en/security/network-security/tor/onionservices-best-practices#installing-and-configuring-onion-services






