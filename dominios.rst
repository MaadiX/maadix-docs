Dominios
========

Primer paso: consigue tu dominio
--------------------------------

Toda web o aplicación necesita un buen dominio. Si ya dispones de un
dominio propio, MaadiX te permite activarlo fácilmente y configurarlo
para usarlo en tus aplicaciones.

Si todavía no dispones de dominio propio, aquí te dejamos algunos
proveedores de ejemplo donde puedes registrarlo de manera sencilla
(¡pero hay muchísimos `más <https://www.icann.org/registrar-reports/accreditation-qualified-list.html>`__!):

-  `gandi.net <https://www.gandi.net/>`__
-  `Njal.la <https://njal.la/>`__

Si ya dispones de un dominio propio, sigue las instrucciones de a
continuación para activarlo.

Recuerda que **MaadiX no es un proveedor de dominios ni ofrece un editor
de zonas DNS.**

Activa tu dominio
-----------------

Activar un dominio desde el panel de control de MaadiX te permitirá
instalar aplicaciones webs y/o utilizar el correo electrónico.

**Para las aplicaciones que se pueden instalar de forma automatizada 
como Jitsi Meet, RocketChat, Only Office Online, Collabora Online,
Discourse, Mailtrain etc, no necesitas añadir el dominio en el panel de
control**, solo indicarlos en el campo requerido para instalación, como
se muestra `aquí </applications/#instalar-aplicaciones>`__.
En el caso de las listas de correo, es necesario añadir los dominios 
a través de Mailman, como se indica `aquí </mail-man/#configurar-dominio>`__.

Para activar un dominio tienes que ir a tu panel de control, a la
sección '**Dominios**' -> '**Añadir un dominio nuevo**'.

Se desplegará un formulario divido en tres secciones:

- Web: toda la información necesaria para activar el servidor web y
  conseguir que el dominio sea accesible desde cualquier navegador.
  Se incluye la opción de instalar Wordpress de forma automatizada.

- Permisos: para otorgar permisos de lectura/escritura a una cuenta,
  normalmente la persona encargada del desarrollo/mantenimiento de 
  la aplicacin web.

- Correo electrónico: activación y configuración del correo electrónico.

.. figure:: img/domains/Add_domain202301.png
   :alt: Añadir dominio

   Añadir dominio

Los campos disponibles y su función se describen a continuación.


Web
***

-  **Nombre del dominio**: introduce el nombre completo de tu dominio o
   subdominio (por ejemplo: example.com o docs.example.com).

Por cada dominio o subdominio que actives, se creará una carpeta en tu
servidor cuya ubicación será ``/var/www/html/example.com/``. Debes subir
tu web o aplicación a esta carpeta para que sea accesible desde el
navegador visitando *example.com*

-  **Activar Servidor Web**: activando esta casilla se creará la configuración
   necesaria para que el dominio sea accesible desde cualquier navegador. 
   En el caso de que solo quieras utilizar el dominio para cuentas de correo 
   electrónico y alojar la web en otro servidor, deja esta casilla desactivada.
   Ten en cuenta que solo podrás activar el servidor web si los DNS para este 
   dominio están apuntando a la IP del ervidor.

   Esto es debido a que todos los dominios se activan con https por defecto 
   creando los certificados con Let's Encrypt. Si no hay correspondencia entre
   la IP del servidor y el registro DNS de tipo A, la creación del certificado fallará.

   Si tienes dudas sobre como crear el registro DNS necesario, consulta el apartado
   `Configura los DNS de tu dominio para que apunten a tu servidor
   <#configura-los-dns-de-tu-dominio-para-que-apunten-a-tu-servidor>`__ de esta página.

-  **Carpeta raíz**:

  Deja este campo en blanco para que la carpeta raíz será /vaw/www/html/example.com/
  En el caso necesites separar la carpeta raíz de otros archivos puedes añadir 
  subcarpetas a este directorio, por ejemplo insertando public_html/web. 
  En este caso la carpeta raíz será /vaw/www/html/example.com/public_html/web/ y es
  donde tendrías que ubicar los archivos de la web. 
  Este valor no se puede editar así que si quieres cambiarlo tendrías que eliminar el 
  dominio y volverlo a crear.

  En caso de dudas, deja el espacio en blanco para una configuración estándard.

-  **Instalar Wordpress**: Activando esta casilla se instalará Wordpress de forma 
   automatizada. En este caso aparecerán dos campos para que introduzcas la contraseña 
   que necesitarás para acceder al panel de administración una vez completada 
   la instalación. Ten en cuenta que solo se podrá instalar Wordpress si se puede activar 
   el servidor web (es decir, si has activado la casilla anterior y la configuración de 
   los DNS es correcta) y si la carpeta raíz está vacía.

-  **PhpPool**: si instalas un Wordpress o cualquier aplicación PhP es muy recomendable que
   le asignes un pool propio, eligiendo uno desde el desplegable.
   De esta forma la aplicación se ejecutará como un proceso separado reforzando así la seguridad.
   El número entre paréntesis indica cuantos dominios están ya asignados al pool. 
   Puedes añadir nuevos pools desde la página  '**Sistema**' -> '**Php-FPM Pools**'.

   En esta paǵina puedes encontrar más información sobre esta funcionalidad:
   `PhP-fpm Pools </php-fpm>`__
  
Permisos
********

-  **Webmaster**: puedes asignar una cuenta webmaster (cuenta
   administradora de la web) en cada dominio o subdominio que actives en
   MaadiX.

Podrá ser webmaster cualquier cuenta ordinaria a la que se le haya
activado el acceso por SFTP o por SFTP+SSH. Haciendo clic en
el desplegable 'Asignar Webmaster' se listarán todas las cuentas
ordinarias que se pueden asignar como webmaster.


En el caso no te aparezca ninguna tendrá que crearlas antes desde la
sección 'Usuarixs' > 'Cuentas ordinarias'. `Aquí </users/#cuentas-ordinarias>`__ puedes leer más sobre las cuentas
ordinarias y qué implican los accesos por SSH y SFTP que se les pueden
activar.

La cuenta webmaster tendrá permisos para crear, borrar o modificar los
archivos dentro de la carpeta ``/var/www/html/example.com/``, donde
podrá crear la aplicación web.

Recuerda que desde la versión 201901, para evitar brechas de seguridad,
ya **no se puede asignar como webmaster la cuenta Superusuarix.**

-  **Activar ACL**: Podrás activar reglas de acceso sobre los archivos del dominio indicando así qué cuentas pueden acceder a ellos y qué privilegios tendrán.

Cuando subes una web, por ejemplo un Wordpress, el propietario de los archivos es quién los ha creado. Así que si has subido los archivos por sftp/ssh utilizando la cuenta Webmaster, esta será la propietaria de los mismos.
En cambio, cuando añades una imagen a la librería de Wordpress o instalas un plugin desde la interfaz gráfica de administración (GUI), estos archivos pertenecerán a la cuenta de Apache (normalmente www-data).
Esto implica que tu cuenta webmaster no tendrá los privilegios necesarios para editar dichos archivos.

Activando las ACL se obvia esta situación otorgando privilegios adicionales sobre un conjuntos de objetos (archivos o directorios). Tanto la cuenta webmaster como Apache tendrán permisos de lectura/escritura sobre sus archivos de forma que podrás editarlos tanto desde la interfaz gráfica como desde el sistema de archivos.

Correo Eletcrónico
******************

-  **Activar servidor de correo para este dominio**: si quieres usar el
   servidor de correo interno para el dominio que estás creando, esta
   opción tiene que estar activada. Si de lo contrario quieres que el
   correo electrónico sea gestionado por otro servidor externo, déjala
   desactivada. Podrás cambiar esta opción en cualquier momento desde la
   página de edición del dominio.

-  **Activar DKIM**: si activas el servidor de correo es muy recomendable
   que actives también esta casilla para conseguir que los correos que envíes 
   no sean tratados como SPAM. Si la activas tendrás también que crear un registro 
   DNS adicional. Consulta la página de instrucciones para la `configuración 
   de DNS </dns>`__ para más detalles.

Configura los DNS de tu dominio para que apunten a tu servidor
--------------------------------------------------------------

Tu servidor incluye un sistema que comprueba automáticamente si tu
dominio está apuntando correctamente a tu servidor. En caso afirmativo,
el mismo sistema procederá con la creación de todas las configuraciones
necesarias. 
Para saber si el proceso de activación y configuración del dominio ha terminado con éxito
consulta el icono **Servidor web** de la página **Ver Dominios**. Si el
icono está en verde (Activado) la configuración para poder crear una web
se ha llevado a cabo satisfactoriamente.

.. figure:: img/domains/web-server-enabled.png
   :alt: Servidor web activado

   Servidor web activado

Para que tu dominio apunte hacia tu servidor, debes modificar sus DNS.
Los servidores DNS (Domain Name System, Sistema de Nombre de Dominios)
son los que transforman los nombres de dominio, pensados para la
comprensión humana, en números que corresponden a las direcciones IP de
las diferentes máquinas conectadas y accesibles públicamente en
Internet.

Haciendo clic en "Ver DNS" en la columna de tu dominio, encontrarás las
configuraciones requeridas para que tu dominio funcione tanto para tu
aplicación web (Registro A) como para tu servidor de correo (Registro MX
SPF y Dkim).

.. figure:: img/domains/view-dns-link.png
   :alt: View DNS

   Ver DNS

Debes introducir estos datos en la sección correspondiente de la
configuración de DNS dentro del **área de cliente de tu proveedor de
dominio** (esta fase tienes que completarla fuera de tu servidor
MaadiX). Seguramente habrá un enlace o pestaña, quizás en el menú, que
diga algo como *DNS*, *Editar registros DNS* o *Editar zona DNS*. Puedes
consultar la sección `DNS </dns>`__ para obtener instrucciones detalladas
sobre los diferentes tipos de registros necesarios para un correcto
funcionamiento de todos los servicios.

.. figure:: img/domains/required-dns.png
   :alt: Required DNS

   DNS requeridos

Una vez hechos los cambios, vuelve a consultar la página de
configuración de DNS en el panel de control de tu servidor MaadiX,
haciendo clic en "Ver DNS" en la columna del dominio. Recuerda que el
proceso de propagación de los nuevos DNS puede tardar hasta 48 horas, de
modo que es normal que durante un tiempo la configuración siga
resultando incorrecta aunque la hayas cambiado.

HTTPS
-----

Todos los dominios que actives a través del panel de control tendrán
siempre activado un certificado SSL y serán accesibles a través de la
dirección: https://tudominio.com

La creación y configuración de los certificados está automatizada y se
completa junto con todo el proceso de activación y configuración de
dominios en tu sistema utilizando `Let's Encrypt <https://letsencrypt.org>`__.

No necesitas llevar a cabo ninguna configuración adicional para activar
HTTPS para tu dominio. Los certificados tienen validez de tres meses y
se renuevan automáticamente.

Subir tu web o aplicación al dominio propio
-------------------------------------------

Una vez aparezca el check verde "Activado" para el Servidor Web de tu
dominio, ya puedes subir los archivos de tu aplicación web a la recién
creada carpeta ``/var/www/html/example.com/``. Puedes hacerlo muy
fácilmente con un cliente SFTP (por ejemplo,
`Filezilla <https://filezilla-project.org/>`__) o a través de una
conexión SSH (por ejemplo, con los comandos scp o rsync), según los
permisos de acceso que tenga la cuenta webmaster asignada a este
dominio. Una vez los archivos estén ahí, podrás visitarlos desde el
navegador en tu dominio *example.com*.

Puedes encontrar más indicaciones aquí: `crea tu web o
aplicación </create-web>`__.

Empieza a usar tu servidor de correo
------------------------------------

Si has activado la casilla 'Activar servidor de correo para este
dominio', también puedes empezar a usar tu servidor de correo
electrónico. Entra en el apartado *Correos > Cuentas de correo* para
abrir nuevas cuentas, haciendo clic en el botón '*Añade una cuenta
nueva*' que encontrarás arriba a la derecha de esta página. Recuerda que
los registros MX y SPF tienen que estar correctamente configurados para
que apunten a tu servidor.

Puedes encontrar más indicaciones aquí: `crea y gestiona cuentas de
correo </email>`__.
