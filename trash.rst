Papelera
========

En la pestaña 'Papelera' podrás encontrar un listado de elementos que puedes eliminar definitivamente del sistema para ahorrar espacio en disco. **Asegúrate bien antes de borrar ya que estos archivos no podrán ser recuperados**.

.. figure:: img/trash/trash.png
   :alt:

   Papelera de reciclaje.


Imágenes Docker
----------------

En esta sección podrás eliminar las imágenes de Docker que no estén siendo utilizadas.

**Advertencia**: si has instalado alguna imagen de docker de forma manual y actualmente no está en uso también aparecerá para ser borrada desde aquí. Asegúrate bien de no borrar una imagen que quieres conservar.



Paquetes obsoletos
------------------

Seleccionando la casilla de esta sección se procederá a la eliminación de paquetes que se hayan instalado previamente pero que ya no son necesarios para el sistema. En concreto, en este proceso se llevará a cabo la ejecución del comando ``apt clean``.


Dominios
--------

Cuando se elimina un dominio desde el panel de control lo que sucede en
el servidor es lo siguiente:

1. Se eliminan el *Virtual Host* y la renovación de certificados de
   Let's Encrypt, para que el dominio ya no sea accesible.
2. Se mueve el directorio ``/var/www/html/example.com`` a
   ``/home/.trash/domains/example.com-timestamp``, para que el contenido
   de la página o aplicación web pueda ser recuperado en el caso de que
   lo necesitas.

Como se observa, al nombre del directorio (``example.com``) se le añade un *timestamp* (``example.com-timestamp``). De esta manera, si el dominio es añadido y borrado varias veces podemos saber a qué versión nos referimos. La fecha del borrado, que es la que se indica en el *timestamp*, se puede ver en la columna de la derecha.

Por seguridad, una vez borrado un dominio, el contenido de
``/var/www/html/example.com`` solo puede ser restaurado entrando por
terminal con la cuenta Superusuarix. A el directorio
``/home/.trash/domains/example.com-timestamp`` se le asignará como owner
``nobody:nogroup`` y solo la cuenta Superusuarix tendrá permisos para
modificarlo.

Si quieres eliminar definitivamente el directorio
``/home/.trash/domains/example.com-timestamp`` puedes marcar 'eliminar'
y 'Aplicar cambios'.

Usuarixs
--------

Cuando se elimina una cuenta desde el panel de control lo que sucede en
el servidor es que el directorio ``/home/user_example`` se mueve a
``/home/.trash/users/user_example-timestamp``, además se le asignará
como owner ``nobody:nogroup``.

Al nombre del directorio ``user_example`` se le añade un *timestamp*
(``user_example-timestamp``) para identificarlo de forma única. Esto nos
sirve para los casos en el caso que una cuenta se crea y se elimina
varias veces. La fecha del borrado, que es la que se indica en el
*timestamp* , se puede ver en la columna de la derecha.

Por cuestiones de seguridad, si se quiere recuperar el contenido de este
directorio se tendrá que entrar por terminal, con la cuenta Superusuarix
para moverlo de ubicación y cambiarle los permisos de owner.

Si quieres eliminar definitivamente el directorio
``/home/.trash/users/user_example-timestamp`` puedes marcar 'eliminar' y
'Aplicar cambios'.


Copias de archivos de aplicaciones
----------------------------------

Cuando se lleva a cabo la actualización de ciertas aplicaciones (por ejemplo, Nextcloud) previamente se hace una copia de seguridad de los datos para prevenir la perdida de información en caso de fallo durante el proceso.

Una vez la actualización se ha realizado con éxito y has comprobado que todo funciona correctamente, puedes seleccionar la casilla de esta sección para proceder a la eliminación de estos archivos y así liberar espacio en disco.
