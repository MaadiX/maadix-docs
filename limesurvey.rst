LimeSurvey
==========

LimeSurvey es una aplicación de software libre y códico abierto que permite la creación de encuestas o formularios en línea. Está orientada tanto a principiantes como a profesionales, ya que facilita una multiplicidad de tipos de respuesta, ramificación a partir de condiciones, plantillas y diseño personalizado. Además, incluye utilidades básicas de análisis estadístico para el tratamiento de los resultados obtenidos.

Su guía de uso oficial la podrás encontrar  `aquí <https://manual.limesurvey.org/LimeSurvey_Manual>`__.

Instalación
-----------

Desde el Panel de Control, ve al apartado '**Instalar Aplicaciones**', ahí podrás ver todas las aplicaciones disponibles para instalar, entre ellas LimeSurvey. Solo tienes que marcar la casilla *'Seleccionar'* e indicar:

- Un **dominio o subdominio** donde lo quieres instalar. Este dominio tiene que estar apuntando a la dirección IP del servidor, para ello tendrás que añadir un registro A en el editor de zonas DNS de tu dominio (operación externa a MaadiX). Recuerda que **este dominio o subdominio no lo tienes que añadir en la sección 'Dominios'**.
- Una **contraseña** para asignarle a la cuenta de administración de LimeSurvey. Guárdala en algún lugar seguro ya que no será enviada por correo por razones de seguridad.

Una vez indicados estos dos parámetros puedes darle al botón de **'Instalar'**.

.. figure:: img/limesurvey/install-lime.png
   :alt:

   Instalación de LimeSurvey.

Después de unos minutos te llegará un email (a la dirección que tengas configurada para la cuenta administración del Panel de Control) confirmando que la instalación terminó correctamente.

Acceso
------

Para acceder a LimeSurvey tendrás que ir al dominio o subdominio que indicaste durante la instalación. También puedes acceder a través del Panel de Control, en '**Mis Aplicaciones**' > '**LimeSurvey**' > '**Ir a la aplicación**'.

Podrás hacer *login* en el panel de administración añadiendo ``/admin`` al final del dominio (por ejemplo: `lime.example.com/admin`) y acceder con la contraseña que tu mismx asignaste durante el proceso de instalación, por razones de seguridad no se envía por correo. **Si olvidas la contraseña** puedes iniciar el proceso de recuperación de contraseña haciendo **clic en '¿Contraseña olvidada?'** e indicando el correo electrónico de la cuenta de administración.

Para que el proceso de recuperación de contraseña funcione debes configurar previamente el envío de emails como se indica más abajo. Te recomendamos hacerlo lo antes posible.

.. figure:: img/limesurvey/acceso.png
   :alt:

   Login en LimeSurvey.


Cambio de dominio
------------------

Si necesitas cambiar el dominio o subdominio de LimeSurvey solo tienes que ir al Panel de Control y en '**Mis aplicaciones**' > '**LimeSurvey**' > '**Configurar**' para indicar el nuevo dominio. Este dominio tiene que estar apuntando a la dirección IP del servidor, para ello tendrás que añadir un registro A en el editor de zonas DNS de tu dominio (operación externa a MaadiX). Recuerda que **este dominio o subdominio no lo tienes que añadir en la sección 'Dominios'**.

.. figure:: img/limesurvey/edit-domain.png
   :alt:

   Cambio de dominio de LimeSurvey.


Configuración de emails
-----------------------

La configuración del envío de email se encuentra en el panel de administración de LimeSurvey. Para acceder debes ir a al dominio de la instalación y añadir ``/admin``, por ejemplo `lime.midominio.com/admin`.

Para configurar los parametros de envío de emails tienes que ir a **"Configuración" > "Global" > "Configuración de correo electrónico"**. Allí puedes marcar la opción SMTP e indicar los parámetros siguientes:

- Servidor SMTP: el nombre de tu servidor seguido de :465 (tuservidor.maadix.org:465).
- Nombre de usuario para la autenticación en el SMTP: una cuenta de correo de tu servidor (contacto@midominio.com).
- Contraseña para la autenticación en el SMTP: contraseña de la cuenta de correo anterior.
- Cifrado SMTP: SSL/TLS.

.. figure:: img/limesurvey/email_sender.png
   :alt:

   Configuración de envío de emails.

Además tendrás que indicar una cuenta de correo como remitente válida en **"Configuración" > "Global" > "Configuración de rebotes de correo electrónico"**.

.. figure:: img/limesurvey/email_from.png
   :alt:

   Configuración de remitente de emails.
