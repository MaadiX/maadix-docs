PHP
===

Versión de PHP
--------------
En la release 202302 la única versión de PhP disponible es la 8.1. Hemos eliminado la 7.4 debido a que ya ha llegado al final de su ciclo de vida.

Owncloud actualmente no tiene soporte para Php 8.1, por lo tanto, si tienes instalada esta aplicación, se desactivará de forma automática cuando actualices el servidor a la release 202302.

Php-Fpm Pools
-------------

PHP-FPM (FastCGI Process Manager, en inglés) es una implementación alternativa al PHP FastCGI 
que logra por un lado reforzar la seguridad y por otro mejorar el rendimiento de aplicaciones Php.

Se trata de separar cada aplicación la una de la otra asignándole un **Pool** de PhP diferente.
Cada Pool tendrá un propietario distinto de forma el usuario de una aplicación php no será capaz 
de leer o escribir archivos de otra aplicación, limitando así la posibilidad de que una web 
infectada acabe comprometiendo a otras.

Para cada `Pool` se generan varios procesos hijos. En MaadiX hemos aplicado la 
configuración conocida como dinámica. Esto significa que el número de procesos hijos 
(child) se asignan de forma dinámica a un determinado Pool en función del número de 
peticiones (visitas) que esté recibiendo una web.
De esta forma el rendimiento mejora ya que se pueden distribuir los recursos del sistema en función 
del uso.  

Por defecto en MaadiX se activan 3 Pools de forma autmática,  para que los tengas disponibles para su 
asignación de forma inmediata. Sin embargo, desde el panel de control, se pueden añadir otros. 
El numero máximo de Pools que se puede crear depende de la cantidad de memoria de la que 
disponga el servidor.


Administrar Pools
*****************

Pra administrar los Pools accede a la página **Sistema** > **PHP-FPM POOLS**.
Desde esta página podrás añadir, borrar o desactivar Pools del sistema.  
En la tabla te aparecerán los Pools existentes y el dominio que tengan asociado (en el caso tuvieran alguno).
Siempre verás como mínimo tres Pools (fpm1, fpm2 y fpm3). Se trata de los tres Pools creados por defecto
y que no podrás eliminar.

Te recomendamos utilizar estos tres Pools antes de añadir otros y no asignar más de una 
aplicación al mismo Pool para así mantenerlas aisladas una de la otra.

En el caso de que necesites crear Pools adicionales puedes hacerlo utulizando el formulario que encuuentras 
al fondo de la página.

.. figure:: img/phpfpm/add-pools.png
   :alt: Añadir Pools 

   Crear nuevo Pools

Para poder eliminar un Pool por completo necesitas antes desactivarlo. Solo podrás desactivar los 
Pool a partir del tercero mientras no tengan ningún dominio asociado. 

Una vez un Pool esté desactivado podrás eliminarlo siempre que no exista un Pool con numeración superior.
Es decir, no podrás eliminar el Pool fpm4 si existe el Pool fpm5. Por esto es buena práctica asignar Pools 
a las aplicaciones PhP de forma secuencial.  

Tal como se muestra en la siguiente imagen, la única opción disponible para el fpm4 será la de reactivarlo.
Para poderlo eliminar se debería antes desactivar y eliminar el fpm5.

.. figure:: img/phpfpm/manage-pools.png
   :alt: Administrar Pools 

   Administrar Pools


Una vez creados, los Pools se pueden asignar a los diferentes dominios que tengas habilitados 
desde el panel de control accediendo a la página de edición de cada dominio. 
Es muy recomendable asignar un único dominio por Pool.

Las aplicaciones Php instaladas desde el panel de control (Nextcloud, Owncloud, Limesurvey, etc) 
ya se configuran de forma automática para que utilicen un pool separado de las otras.




