Disco Cifrado
=============

La opción de solicitar el cifrado de disco añade capas de complejidad en la administración 
del servidor ya que cada vez que se reinicia el sistema es necesario desencriptar la unidad 
y solo se puede hacer accediendo por SSH.

Si has solicitado esta opción no podrás acceder a la funcionalidades completas del panel de 
control hasta haber cambiado las contraseñas para desbloquear el disco.

El mismo panel de control detecta si ya has efectuado este paso y en caso contrario te 
redirigirá a la página correspondiente.

El panel de control permite asignar 2 contraseñas para las dos primeras ranuras (Slot 0 y Slot 1)
aunque LUKS2 permita muchas más:
https://unix.stackexchange.com/questions/553691/how-many-key-slots-does-the-luks2-support. 
Puedes añadir más Slots desde la consola si lo deseas.

.. figure:: img/luks/update-luks-passphrase.png
   :alt: Add pgp key

Tendrás que elegir dos nuevas contraseñas que tendrán que tener un mínimo de 32 caracteres y no 
pueden incluir caracteres especiales como
``{[*$^`` etc.

Esta limitación es debida a que podría darse el caso de que el idioma de tu teclado no corresponda 
con el de la consola para el desbloqueo del disco lo que provocaría una falta de coincidencia entre 
la contraseña que insertas y la que realmente recibe el sistema.

Por esta razón en el formulario se proporciona una sugerencia de contraseña para cada Slot además de 
un botón para copiar toda la cadena, con el fin de evitar errores. 

Tendrás que confirmar la contraseña para cada Slot y sobre todo copiar y guardar cada una en un lugar seguro.
Recuerda que **si pierdes estas contraseñas no podrás acceder al sistema** una vez se reinicie y 
**el equipo de MaadiX no tendrá forma de solucionarlo**.

Como último paso tendrás que introducir la contraseña que utilizas para acceder al panel de control.

.. figure:: img/luks/confirm-luks-passphrase.png
   :alt: Add pgp key 

Una vez le des al botón 'Confirmar' el sistema configurará las dos nuevas contraseñas que acabas de elegir 
y cuando termine (al cabo de unos tres minutos) reiniciará de forma automática el servidor.

Esto te permitirá averiguar si puedes descifrar el disco para arrancar el sistema antes de insertar cualquier dato.
Para ello, tendrás que acceder por SSH al servidor por el puerto 2000 con el comando:
    
    ``ssh -p 2000 root@myserver.maadix.org``
    sustituyendo myserver.maadix.org  por el nombre de tu servidor, o bien:

    ``ssh -p 2000 root@XX.XX.XX.XX``
    sustituyendo XX.XX.XX.XX  por la IP de tu servidor

Si el servidor ya se ha reiniciado verás una consola con este mensaje:
    
    ``Please unlock disk cryptroot:``
        
Inserta una de las dos contraseñas y dale al ENTER
    
Si en la consola aparecen los siguientes mensajes:

    ``cryptsetup: cryptroot set up successfully
    Connection to mysqerver.maadix.org closed.``

Significa que el disco se ha podido descifrar y el sistema está iniciándose. 
Tu conexión al puerto 2000 se ha cerrado y para volver a entrar por ssh tendrás 
que utilizar el puerto que tengas configurado (por defecto el 22).

En este punto puedes reiniciar el servidor para comprobar la segunda contraseña.
  
Backup
------

Si la cabecera de un volumen LUKS se daña, todos los datos se pierden de forma permanente 
a menos que exista una copia de seguridad de la cabecera. 
Si se daña una ranura de claves, sólo puede restaurarse a partir de una copia de seguridad 
de la cabecera o si existe otra ranura activa con una frase de contraseña conocida y que 
no esté dañada. 
Dañar la cabecera de LUKS es algo que puede ocurrir con sorprendente frecuencia. 

Hay dos componentes críticos para el descifrado: La propia cabecera y los Slots. 
Si la cabecera se daña y no se dispone de una copia de seguridad, no se puede hacer nada para 
acceder a los datos.
Si se daña uno Slot, se puede acceder a los datos si existe otro Slot que no esté dañado.
 
Por esto es muy importante disponer de más de un Slot y hacer una copia de respaldo de la cabecera.
 
En MaadiX el volumen LUKS es /dev/vda2 y puedes hacer una copia de la cabecera con el siguiente comando:
     
``sudo cryptsetup luksHeaderBackup /dev/sda2 --header-backup-file /root/sda2-header-backup``

Tendrás que copiar el archivo /root/sda2-header-backup a un dispositivo externo

Puedes consultar más detalles en esta guía: 
https://www.lisenet.com/2013/luks-add-keys-backup-and-restore-volume-header/
