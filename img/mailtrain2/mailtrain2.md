Mailtrain
=========

Mailtrain es una aplicación de código abierto que permite crear campañas
de email sin tener que utilizar servicios de terceros, preservando así
la privacidad de nuestra comunidad. Ofrece una interfaz muy fácil e
intuitiva para crear nuevas listas, configurar parámetros de suscripción
y envío, así como muchísimas otras opciones útiles. Todas las
instrucciones de uso, están incluidas en la misma herramienta. En este
tutorial, nos limitamos a dar indicaciones para la configuración general
de la herramienta, para que pueda conectar con el servidor de correo y
enviar los mensajes a las listas.


Instalación
-----------

Desde el Panel de Control, ve al apartado '**Instalar Aplicaciones**', ahí podrás ver todas las aplicaciones disponibles para instalar, entre ellas Mailtrain 2. Solo tienes que marcar la casilla *'Seleccionar'* y rellenar los campos que aparecen. Mailtrain2 necesita tres dominios/subdominios para su instalación. Todos ellos tendrán que tener una entrada DNS de tipo A o CNAME válida, que apunte a la IP de tu servidor. Para ello tendrás que añadir un registro A en el editor de zonas DNS de tu proveedor de dominio (operación externa a MaadiX). Puedes consultar la sección DNS para más información.
Recuerda que los dominios que utilizas para instalar aplicaciones  **no se tienen que añadir en la sección 'Dominios'**.


- **Dominio de administración**  Este será el dominio que utilizarás para acceder a la interfaz de administración para crear campañas, templates, gestionar envíos etc.

- **Dominio Sandbox** Este dominio está en un segundo plano y nunca accederá directamente a él. La aplicacioçon lo necesita para poder cargar las plantillas de las newsletters.
- **Dominio público** Este dominio se utiliza para construir los enlaces públicos que se incluyen en los mensajes que envías, por ejemplo, para darse de baja o suscribirse.
- **Importar datos desde mailtrain 1?** Este campo solo aparece si ya tienes instalada la versión 1 de Mailtrain y te permite importar los datos anteriores a esta nueva instalación. Consulta la sección `Datos importados`_ para más detalles. Se conservarán de forma simultánea ambas versiones y aparecerán como Mailtrain y Mailtrain 2.
- Una **contraseña** para asignarle a la cuenta de administración de Mailtrain. Guárdala en algún lugar seguro ya que no será enviada por correo por razones de seguridad.

Una vez indicados estos parámetros puedes darle al botón de **'Instalar'**.

.. figure:: img/mailtrain2/install.png
   :alt:

   Instalación de Mailtrain.

Después de unos minutos te llegará un email (a la dirección que tengas configurada para la cuenta administración del Panel de Control) confirmando que la instalación terminó correctamente. Si has elegido la opción de importar datos desde tu instalación anterior, el proceso es bastante más largo.

Configuración
-------------
Una vez hecha la instalación puedes acceder a la interfaz de administración utilizando el Dominio de administración , en nuestro ejemplo mailtrain.example.com. El enlace está disponible desde el menú lateral del panel de control en Mis Aplicacioneas -> Mailtrain2 -> Configuración.  
Si has elegido la opción de importar los datos consulta también la sección `Datos importados`_.


Desde la página  'Administración' -> 'Ajustes Globales' inserta la dirección bajo la que se puede
visitar tu instalación de Mailtrain, es decir el dominio de instalación
de la aplicación, en nuestro ejemplo: *https://news.example.com* También
tendrás que modificar el parámetro 'Admin Email' para que se puedan enviar
correctamente notificaciones del sistema.


Configuraciones de Envío
~~~~~~~~~~~~~~~~~~~~~~~~


Mailtrain2 ofrece dos sistemas diferentes para enviar los boletines:
- ZONEMTA
- SMTP

Cada una tiene sus ventajas y desventajas. Puedes configurar cual de ellas quieres utilizar accediendo a la pestaña 'Administración' -> 'Configuraciones de envío del menú superior.

ZONEMTA:  

Es la opción que encontrarás por defecto a menos que hayas importado los datos de tu instalación anterior.

Ventajas:
Con esta opción tendrás activado el control de rebotes, es decir, podrás consultar culaes correos no se han podido entregar.

Desventajas:
Para minimizar el riesgo de que tus correos sean rechazados o acaben en la carpeta de correo no deseado, tendrás que añadir manualmente la clave privada DKIM del dominio correspondiente a la cuenta 'From'.
Si la cuenta 'From' que vas a utilizar para enviar los boletines es una cuenta creada en tu servidor MaadiX, pongamos info@example.com, puedes acceder a esta clave, pero solo desde la consola como Superuser.
Verás que en la misma interfaz sale la alerta en amarillo:  Do not use sensitive keys here. The private key is not encrypted in the database.
El problema es que cualquier clave privada es un dato sensible, así que si quieres utilizar este método de envío no tienes otra opción.

Para copiarla tendrás que hacer lo siguiente:

- Accede al servidor por SSH (No SFTP) con el comando
ssh mysuperusername@myserver.maadix.org

sudo cat /etc/opendkim/keys/example.com/default.private

Te pedirá la contraseña de la cuenta mysuperusername.
Una vez insertada, te aparecerá la clave DKIM  en la consola.

-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEA2hVMy9fdL8319pW2G0pk9JkyJl9gl+6+OvF6Q0fu79V9WTFg
etdKendHvu+/Oex3jF3lckj2dqI0l0cycAfLjQrX4w+IZlkLT6pkjVVzpIvP7kaE
vu0SbYwQGzP1CTAq95RBY5dggUqWK4JQTc9CLw5D6SNnx8LzmWMQTAsH/2puqtpA
/Kc3iYHyrkSs9KOFE7P3iK7hTAOnCT6QQpjcymrFzhYMD9xpbKkn7zYpjMASBM1S
dhw74iahMx7VDXrq2y40r7lbAFIpx6bwUKnTC3Po4etOshwXk4PnMZMskH2RF2ZJ
PmlZetGZdc+NbcE+872sWK/nsE3wz8jKJOI8CQIDAQABAoIBACLkwZLkCRn7BF3d
+nNHZfvU/zcwj43/vyRDOuLFAbASIqpAwkUlaV9K8onUZm5oLYCiDXJJxlWZAn0q
ASH3vVPl5kEIXTSzDqJCizAp+gTb8fsYAWljGU252WHg4FYGBPn0GLpMoD5PptNz
WFH61YYd7DPDXHmT8dIbDaJ9KVxuaWqh1FeQXx4Ny4N2H2h8nnBUa9xvx+Y+5x3v
GyGHsCuX0/sY4pPraVzqVgs8zdQuBdlIpyqvCVM0BxQ8+V/llPCvwd0d7EDhqJ+x
NG40YWECgYEA+Sv7By63osGJulgOyvlUQ6xqayV/JsbZoaE3xJXZ+JNW/rwJFQKh
nwXJmz656oS+ZzxdfD33Uj0GwzyyyHMrJumQVmwPfbpQSATJNQbH6tb5Up+71rfv
43EIXYRNa7rRQL0s68LXinUukk84ojMB2oTHkj5mo6HKMhgyOXcsxe0CgYEA4A85
F7BtS0k9wqIMNkqBn1LOyun1LUNTZKW7kOSvs9xWHDY256je3o9n2nyGu76+bFBB
U3eznAc2k5dcXndw0vq6QmtJW3fyamcZZtOm+uxchRkAzEgmUCdGY/4oetdvOwgu
oVlfJzxSJeONUv3Dt/ELHrCT6c8WXB61Wfgv2M84ap7ePPEHF/SZQhyNmth0jqlU
PgSXgDTFtqvYoCeDUDSOcfp7BXae4ym9tr9CaQosRFOEnm3xT1HGXG5pJqVBUxB1
ezIajWbJZ4x7NaW3kjrqRQKBgBRwDwskIrQNWD7avTZukWhRC1/i+2RHkeHKZFOR
KJ3FaW1lXJjBBKNUfWqmlTum6IPExlCwyIdg+Q95bQmI55e3GypX0vcu98PQ57ZD
fvY5AoGBAMkt+yazrmJ0qFiXNU5Wl8TFb4em2OIitfQ4WZgqkwn0n0v49ZRFv383
Oe3t6hwm1Iwsu3CdMIO3ftCmqfpmnqc295mIHquwh3SMpGKcLnm3vMo/3godAdny
bezGE/Qo3dn99bLzZeI/PhlbE0A39Z0YHbfSUooPhMOVTOpRqOxq
-----END RSA PRIVATE KEY-----

Copia todo el texto incluidas las líneas que contienen BEGIN y END PRIVATE KEY e insertalo en el campo DKIM private key.

En el campo DKIM domain tendrás que insertar example.com y en DKIM key selector default._domainkey

.. figure:: img/mailtrain2/dkim-zonemta.png.png
   :alt: Mailtrain DKIM for ZONEMTA


SMPT:

Ventajas:
- No será necesario copia la clave DKIM, que se utilizará de forma automática para los envíos, siempre que esté activada para el dominio de la cuenta 'From'.

Deventajas:

No tendrás control de rebotes desde la interfaz. Aun así podrías tener constancia de cuales destinatarios han rechazado el mensaje accediendo a   la cuenta 'From' con un cliente de correo (Thunderbird, Rainloop, etc).

Para configurar el envío por SMPT tendrás que elegir la opción SMPT Genérico con autenticación , rellenando los campos Hostname, Port y añadiendo  usuarix y contraseña. Los valores necesarios dependerán del proveedor de correo de la cuenta de envío. En la siguiente imagen ponemos un ejemplo de configuración de envío por SMPT con autenticación para la cuenta info@example.com creada en un servidor MaadiX myserver.maadix.org

.. figure:: img/mailtrain2/SMTP-auth-settings.png
   :alt: Mailtrain 2 SMPT auth settings






Datos importados
----------------

Desde la release 202302 de MaadiX la versión disponible de Mailtrain es la 2.
Hemos desarrollado un mecanismo de importación de datos desde la versión 1 a la 2. Sin embargo, debido a la gran dferencia de estructura entre las dos versiones, no es infalible, dependiendo de tu configuración anterior.
En el momento de instalar Mailtrain2 puedes probar la importación automatizada activando el checkbox 'Importar datos desde Mailtrain 1'

En este caso tendrás las dos aplicaciones instaladas de forma simultánea. Podrás comprobar que todos los datos se hayan importado correctamente y hacer los retoques manuales necesarios. En la nueva interfaz encontrarás las campañas y las listas creadas anteriormente así como las configuraciones generales de envío. Sin embrago es necesarios que revises manualmente algunas secciones que detallamos a continuación.

Envío
~~~~~

En la sección Administración -> Configuraciones de envío encontrarás como valores por defecto los de tu configuración anterior. Sin embargo es necesario que hagas una test de envío ya que, dependiendo de tus preferencias anteriores, es posible que Mailtrain2 no consiga enviar los correos.

En la siguiente imagen puedes ver un ejemplo de configuración de SMTP Genérico sin autenticación.

.. figure:: img/mailtrain2/SMTP-settings.png
   :alt: Mailtrain 2 SMPT generic settings


 Este tipo de configuración no funciona en Mailtrain2 así que, para seguir utilizando SMPT, tendrás que modificarla para que use SMTP con autenticación , modificando los campos Hostname, Port y añadiendo  usuarix y contraseña. Los valores necesarios dependerán del proveedor de correo para la cuenta de envío. En la siguiente imagen ponemos un ejemplo de configuración de envío por SMPT con autenticación para la cuenta info@example.com creada en un servidor MaadiX myserver.maadix.org

 .. figure:: img/mailtrain2/SMTP-auth-settings.png
    :alt: Mailtrain 2 SMPT auth settings



Plantillas
~~~~~~~~~~

Algunas plantillas importadas podrían no funcionar correctamente. Antes de reutilizarlas para nuevos envíos, accede a su edición y dale a Guardar (aunque no cambies nada en ellas). Igualmente antes de utilizarla, te recomendamos hacer un test de envío.

Dominios y enlaces
~~~~~~~~~~~~~~~~~~

Si lo deseas puedes volver a utilizar tu dominio anterior para la nueva instalación de Mailtrain2. Para ello será necesario eliminar la instancia previa ya que no es posible tener dos aplicaciones activas bajo el mismo dominio. Si decides proceder, ten en cuanta lo siguiente:

- Una vez eliminada la instalación anterior ya no podrás recuperarla. Otra opción que tienes, en lugar de desinstalarla, es cambiar el dominio de la instalación antigua para poderlo utilizar en la nueva. Este dominio correspondería al Dominio publico en Mailtrain2.
- Los enlaces internos incluidos en tus newsletters enviadas anteriormente (bajas, altas, etc) no funcionarán en la nueva instalación ya que la estructura de la aplicación ha cambiado sustancialmente.
- En la página ‘Settings’ inserta la dirección bajo la que se puede
visitar tu instalación de Mailtrain, es decir el dominio de instalación
de la aplicación, en nuestro ejemplo: *https://news.example.com* También
tendrás que modificar el parámetro 'Admin Email' para que se puedan enviar
correctamente notificaciones del sistema.
