Dolibarr
==========

Dolibarr ofrece las funcionalidades de un ERP (Sistemas de Planificación de Recursos de la Empresa, en inglés Enterprise Resource Planning) y de un CRM (Sistema para la Administración de Relaciones con el Cliente, en inglés Customer Relationship Management). Facilita la gestión de contactos, facturas, inventario, pedidos, pagos, etc. a través de módulos que se van activando según las necesidades de cada proyecto. Destaca por ser sencillo y estar orientado a pequeñas/medianas empresas o asociaciones.

Podrás encontrar más información en su `web <https://www.dolibarr.es/index.php>`__ y probar una demo `aquí <https://www.dolibarr.es/index.php/dolicloud>`__.

Instalación
-----------

Desde el Panel de Control, ve al apartado '**Instalar Aplicaciones**', ahí podrás ver todas las aplicaciones disponibles para instalar, entre ellas Dolibarr. Solo tienes que marcar la casilla *'Seleccionar'* e indicar un **dominio o subdominio** donde lo quieres instalar y darle al botón de **'Instalar'**. Este dominio tiene que estar apuntando a la dirección IP del servidor, para ello tendrás que añadir un registro A en el editor de zonas DNS de tu dominio (operación externa a MaadiX). Recuerda que **este dominio o subdominio no lo tienes que añadir en la sección 'Dominios'**.

.. figure:: img/dolibarr/install.png
   :alt:

   Instalación de Dolibarr desde el Panel de Control.

Posteriormente tendrás que ir al dominio o subdominio que especificaste para la instalación y seguir los pasos que se van indicando.

.. figure:: img/dolibarr/asistente.png
   :alt:

   Inicio del asistente de instalación de Dolibarr.

En el proceso se te pedirá asignar una contraseña para la cuenta de administración, guárdala en algún seguro ya que no podrás recuperarla hasta configurar correctamente los parámetros para el envío de correos electrónicos que se indican posteriormente.

.. figure:: img/dolibarr/pass.png
   :alt:

   Creación de contraseña de la cuenta de administración.

Acceso
------

Para acceder a Dolibarr tendrás que ir al dominio o subdominio que indicaste durante la instalación. También puedes acceder a través del Panel de Control, en '**Mis Aplicaciones**' > '**Dolibarr**' > '**Ir a la aplicación**'.

Podrás hacer *login* con la cuenta de administración y la contraseña que tu mismx asignaste durante el proceso de instalación, por razones de seguridad no se envía por correo. **Si olvidas la contraseña** puedes iniciar el proceso de recuperación de contraseña haciendo **clic en 'Forgotten password?'** e indicando el nombre de la cuenta de administración.

Para que el proceso de recuperación de contraseña funcione debes configurar previamente un correo asociado a esta cuenta y los parámetros para el envío de emails como se indica más abajo. Te recomendamos hacerlo lo antes posible.

.. figure:: img/dolibarr/access.png
   :alt:

   Login en Dolibarr.


Configuración de emails
-----------------------

En primer lugar se recomienda añadir un email a la cuenta de administración para que en caso de olvido de la contraseña se pueda enviar un correo para su recuperación. Para ello hay que ir a la configuración de la cuenta en "**Nombre de la cuenta**" (arriba a la derecha)> "**Card**" > "**User**" y hacer clic en "**Modify**". En la sección Email añadir una cuenta de correo y hacer clic en "**Save**".

En segundo lugar, la configuración del envío de email se encuentra **"Setup"** (Menú de la izquierda) > **"Emails"** y en la sección "Outgoing emails" tendrás que indicar los parámetros siguientes:

- Email sending method: SMTP/SMTPS socket library.
- SMTP/SMTPS Host: el nombre de tu servidor (tuservidor.maadix.org).
- SMTP/SMTPS Port: 465.
- SMTP ID: una cuenta de correo de tu servidor (contacto@midominio.com).
- SMTP Password: contraseña de la cuenta de correo anterior.
- Use TLS (SSL) encryption: yes.
- Use TLS (STARTTLS) encryption: No
- Authorise les certificats auto-signés: No
- Sender email for automatic emails: la cuenta de correo de tu servidor indicada en SMTP ID (contacto@midominio.com).

Estas indicaciones son para hacer uso de un servidor de correo instalado en tu mismo servidor. Los parámetros son similares a los que indicarías al configurar un cliente de correo como se indica en la `documentación sobre emails </email/#cliente-de-correo>`__ (solo la parte referida a correo saliente SMTP).

Si prefieres configurar otro servidor de correo para el envío de emails, puedes leer sobre ejemplos de configuración habitual para Gmail, iCloud, Gandi, Outlook, etc en su `documentación oficial <https://wiki.dolibarr.org/index.php/Setup_EMails#Examples_for_common_Mail_server_setup>`__.

.. figure:: img/dolibarr/email.png
  :alt:

  Configuración de envío de emails.
