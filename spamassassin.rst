SpamAssassin
============

Esta aplicación permite reducir la cantidad de correos no deseado. 
Analiza todos los mensajes que recibes y les asigna una puntuación (Score) que determina si han de ser etiquetados como Spam o no.

Por defecto, una vez instalada la aplicación, el umbral de puntuación estará definido en 5. Es decir, cualquier correo que recibas y que tenga una puntuación spuerior a 5, se guardará en la carpeta de correos no desdeados. 
Una vez instalado SpamAssassin puedes modificar este valor tal como explicamos a continuación.

Cambiar Score
-------------

Para modificar el embral del Score de SpamAssassin, accede a la pestaña Mis Aplicaciones -> SpamAssassin -> Configurar donde encontrarás un formulario como el que se muestra en la siguiente imagen:

.. figure:: img/spamassassin/score.png
   :alt: Configurar Score


El valor por defecto de SpamAssassin (5) podría resultar demasiado laxo provocando que mucho correo no deseado siga llegando a tu bandeja de entrada. 

Puedes probar diferentes valores con tal de reducir aun más el correo no deseado, teniendo en cuenta que si aplicas una política demasiado restrictiva podrías generar la situación contraria, etiquetando correo legítimo como Spam.

Contra más baja sea la puntuación que que recibe un correo después de ser analizado por SpamAssassin, mejor será su calidad.
Si configuras el umbral para el Score de Spammassin con un valor inferior a 5 estarás aplicando una política más restrictiva comparada con la que viene por defecto.
Por ejemplo, si aplicas un Score de 4 y alguien te envía un correo que alcanza una puntuación de 4.1 será marcado como Spam.
A medida que bajes el valor del Score estarás requiriendo más calidad para los correos que recibes.

Nuestra recomendación es que vayas decrementando poco a poco el valor Score y que cada vez que lo modifiques observes cuales correos se están marcando como Spam, asegurándote de no estar rechazando correos legítimos.

