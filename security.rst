Seguridad
=========

En el panel de control, en la pestaña 'Sistema', encontrarás la sección
'Seguridad'. Aquí vas a poder configurar diferentes opciones para
reforzar la seguridad de tu servidor.

SSH
---

En el apartado 'SSH' podrás **configurar el puerto para las conexiones
SSH**. Elige del desplegable el puerto que quieras entre el 2001 y el
2010 (por defecto, tendrás el puerto 22). Una vez cambiado, las
conexiones por SSH tendrán que incluir el parámetro con el nuevo puerto
designado, por ejemplo:

``ssh -p 2001 user@minombreenmaadix.maadix.org``

Cambiar el puerto de la conexión SSH refuerza tu seguridad porque la
mayoría de atacantes hacen intentos de conexión al puerto 22 (el puerto
por defecto para SSH).

Además, puedes marcar la casilla '**Deshabilitar acceso SSH con
contraseña**' para que las conexiones al servidor solo puedan hacerse
mediante claves SSH, no con contraseña.

Esta opción protegerá de cualquier intento conexión por SSH por parte de
cuentas desconocidas que traten de probar diferentes contraseñas.

Después de deshabilitar acceso SSH con contraseña solo las cuentas a las
que les hayas añadido su clave pública (Usuarixs > Cuentas ordinarias >
Clave SSH) van a poder conectase al servidor. Esto aplica tanto a
conexiones por SSH o como por SFTP.

Recuerda que puedes añadir la clave pública de una cuenta tanto al
crearla como al editarla. A la cuenta Superusuarix también le puedes
añadir clave SSH. Desde MaadiX, lo recomendamos especialmente para esta
cuenta.

En el caso de que no deshabilites el acceso con contraseña se va a poder
acceder al servidor haciendo uso tanto de contraseña como de clave SSH.
Esto puede ser cómodo para que lxs usuarixs no tengan que tipear la
contraseña pero no añadirá seguridad al sistema.

Si tienes dudas sobre como generar y utilizar claves SSH puedes
consultar `este
tutorial <https://desarrolloweb.com/articulos/crear-llaves-ssh.html>`__.

.. figure:: img/security/ssh-sec.png
   :alt:

   Apartado de seguridad para SSH.

TLS
---

TLS (en inglés: `Transport Layer
Security <https://es.wikipedia.org/wiki/Seguridad_de_la_capa_de_transporte>`__)
es un protocolo criptográfico que proporciona comunicaciones seguras en
internet. Hay diferentes versiones de este protocolo (TLS 1.0, TLS 1.1,
TLS 1.2) y actualmente `lo
recomendado <https://www.packetlabs.net/tls-1-1-no-longer-secure/>`__ es
utilizar TLS 1.2. Las versiones anteriores se consideran inseguras ya
que contienen vulnerabilidades de seguridad, corregidas en la versión
TLS 1.2.

En el apartado **Correo electrónico** puedes configurar qué versiones de
TLS quieres que tu servidor de correo admita.

.. figure:: img/security/mail-sec.png
   :alt:

   Apartado de seguridad sobre versiones TLS (correo electrónico).

Aunque se recomienda usar solo usar TLS 1.2, se da la opción de también
soportar versiones anteriores. Esto da la posibilidad de recibir o
enviar correos a servidores que hagan uso de estas versiones (por
ejemplo, servidores de correo antiguos que aún no tengan habilitado TLS
1.2).

Puedes cambiar las versiones TLS soportadas cuando lo desees.

De la misma manera, en el apartado **Servidor web** vas a poder
establecer qué versiones de TLS quieres que tu servidor web soporte.
Igualmente, solo TLS 1.2 es la opción recomendada.

.. figure:: img/security/web-sec.png
   :alt:

   Apartado de seguridad sobre versiones TLS (servidor web).

En este caso, serán los navegadores web los que tendrán que soportar TLS
1.2 para poder consultar las páginas webs alojadas en tu servidor. Esto
será así en la mayoría de navegadores, solo en caso de navegadores
antiguos que solo soporten versiones anteriores se podrían dar
problemas.

Se da la opción de elegir entre las versiones soportadas para adaptarse
a todas las necesidades. Podrás cambiar esta configuración cuando lo
desees.

Hasta que no le des al botón 'Guardar' no se realizarán los cambios.

Rkhunter
--------

Rkhunter es una herramienta para detectar rootkits, puertas traseras y otro malware. Funciona básicamente escaneando los archivos importantes del sistema y comparando sus hashes con una base de datos en línea para asegurar que no han sido modificados. También comprueba los permisos de los archivos, busca archivos ocultos, cadenas dudosas en módulos del kernel, directorios por defecto de rootkits, etc.

Puedes leer más detalles en su `página web oficial <http://rkhunter.sourceforge.net/>`__ y ver el código en su `repositorio <https://sourceforge.net/p/rkhunter/rkh_code/ci/develop/tree/>`__.

Cuando **Rkhunter detecta alguna modificación sospechosa en el sistema envía una alerta por correo electrónico** en el que se listan los cambios que Rkhunter ha detectado.

Rkhunter informa si un archivo ha cambiado, pero no da información sobre lo que ha causado el cambio. Por esta razón podrían generarse falsos positivos debidos a la instalación o actualización de algún paquete que Rkhunter registra como anómalos

**¿Como reconocer cambios legítimos?**

Falsos positivos habituales
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Los falsos positivos pueden darse tanto en el momento de la instalación de nuevos paquetes o aplicaciones, como en el proceso de actualización.

Instalaciones de aplicaciones desde el panel de control
"""""""""""""""""""""""""""""""""""""""""""""""""""""""
Algunas aplicaciones necesitan la creación de unx usuarix adicional durante el proceso de instalación. Cuando en el sistema se añade una nueva cuenta, rkhunter envía una alerta, sin tener en consideración el origen de esta modificación.

Así que después de instalar ciertas aplicaciones  desde el Panel de Control,  podrías recibir un correo alertando de que se han creado nuevxs usuarixs y grupos en el sistema.

En la siguiente tabla tienes un listado de usuarixs y grupos que se crean por cada aplicación. Si después de instalar algunas de ellas recibes una alerta conforme se ha creado una nueva cuenta y hay coincidencia con esta tabla puedes obviar la alerta.

+----------------------------+------------+------------+
| Aplicación                 |  Usuarix   |  Grupo     |
+============================+============+============+
| Etherpad                   |  etherpad  | etherpad   |
+----------------------------+------------+------------+
|Jitsi                       |  jicofo    |   jitsi    |
+----------------------------+------------+------------+
| Jitsi                      |   jvb      |    jitsi   |
+----------------------------+------------+------------+
|Jitsi                       |  prosody   |  prosody   |
+----------------------------+------------+------------+
| Libre Office Online        |    lool    |   lool     |
+----------------------------+------------+------------+
|Mailman                     |  mailman   |  mailman   |
+----------------------------+------------+------------+
|Mailtrain                   | mailtrain  | mailtrain  |
+----------------------------+------------+------------+
|MongoDB                     |  mongodb   | mongodb    |
|(Dependencia de Rocketchat) |            |            |
+----------------------------+------------+------------+
| Rocketchat                 | rocketchat |rocketchat  |
+----------------------------+------------+------------+
|coturn                      | turnserver |turnserver  |
+----------------------------+------------+------------+
|Spamassassin                |debian-spamd|debian-spamd|
+----------------------------+------------+------------+


Aquí mostramos un ejemplo de correo enviado por Rkhunter después de instalar Jitsi, donde se puede apreciar las alertas causadas por la creación de diferentes usuarixs y grupos.

Ejemplo de correo::

  Warning: User 'jvb' has been added to the passwd file.
  Warning: User 'prosody' has been added to the passwd file.
  Warning: User 'jicofo' has been added to the passwd file.

  Warning: Changes found in the group file for group 'ssl-cert':
  User 'prosody' has been added to the group
  Warning: Group 'jitsi' has been added to the group file.
  Warning: Group 'prosody' has been added to the group file.

Las tres cuentas jvb, prosody y jicofo son cuentas necesarias para la instalación de Jitsi, tal como puedes comprobar en la tabla anterior, así que puedes tranquilamente `silenciar esta alerta </security/#silenciar-alertas>`__ desde el panel de control, para que Rkhunter deje de molestar con esto.

En el caso de de no haber coincidencia, deberías asegurarte del origen del cambio notificado antes de silenciar la alerta.

Actualizaciones
""""""""""""""""

En MaadiX hay activado un sistema de actualizaciones automáticas (Unattended Upgrades), que se ejecuta periódicamente y mantiene el sistema actualizado. 
Es posbile que te llegue alguna alerta debida a paquetes que se han actualizado automáticamente, como por ejemplo ldd o syslogd. Esto es debido a que durnate un periodo (en las release 202103-202201) la configuración de rkhunter provocaba alertas debidas a las actualizaciones nautomáticas. 

Si actualizas MaadiX desde el panel de control aplicarás una nueva configuración del sistema en la que los cambios generados por Unattended Upgrades no provocarán alertas por parte de rkhunter.

Sin embargo, si ejecutas una actualización de forma manual con el comando ``apt-get upgrade``, entonces sí podrías recibir las alertas. Este sería un ejemplo de correo que podrías recibir en este caso, donde se actualizado el paquete curl::

  Warning: The file properties have changed:
  File: /usr/bin/curl
  Current hash: 0e419fccc547bae9ff7fe2e7afa9cee381ce7305224f00a00bbd93fc087c600c
  Stored hash : f180baed99bb09992aa231912c71254c637abb8993d2f52093c7711c06c9ae68
  Current inode: 6689779 Stored inode: 6684934
  Current file modification time: 1617137760 (30-mar-2021 22:56:00)
  Stored file modification time : 1582383706 (22-feb-2020 16:01:46)


Revisión Manual
^^^^^^^^^^^^^^^^^^^

Además de revisar las alertas por email, puedes también comprobar el estado del sistema con los comandos:

``sudo rkhunter --check``

``sudo rkhunter --check --report-warnings-only`` (solo muestra las alertas)

Estos comandos devuelven los resultados de las comprobaciones que Rkhunter ejecuta. En esta captura mostramos un ejemplo de lo que devuelve el comando desde la terminal, en un sistema donde no se ha detectado ningún archivo sospechoso:

.. figure:: img/security/rkhunter-satus-ok.png
   :alt:

   Ejemplo de comprobación de Rkhunter por terminal (estatus ok).


Supongamos ahora que aparece un archivo sospechoso en el directorio ``/etc``. Para el ejemplo hemos creado el archivo oculto ``.IamBad`` en el mismo directorio. Si volvemos a ejecutar el comando ``rkhunter --check`` vemos como el resultado cambia y nos aparece la alerta correspondiente:

.. figure:: img/security/rkhunter-suspicious-file.png
   :alt:

   Ejemplo de comprobación de Rkhunter por terminal (archivo oculto sospechoso).


Así que **si recibes un correo** o al ejecutar el comando anterior ves una alerta **que no reconoces** (por ejemplo, que **existen nuevos archivos ocultos en rutas como** ``/etc``), entonces **sí deberías preocuparte**.

También puedes revisar los logs en ``/var/log/rkhunter.log``.


Silenciar Alertas
^^^^^^^^^^^^^^^^^

Desde el panel de control tienes la opción de aprobar las alertas que Rkhunter ha detectado, para que deje de enviarlas por correo. Remarcamos que **antes de utilizar esta funcionalidad se recomienda revisar bien las alertas** hasta confirmar que los cambios que se han generado en el sistema corresponden a alguna instalación, configuración o actualización que hayas llevado a cabo previamente.

Una vez hecha esta comprobación,  **si  Rkhunter está alertando de un cambio que es legítimo** (por ejemplo, tras realizar alguna instalación o configuración) puedes ir al Panel de Control, a la sección "Seguridad" y **hacer clic en el botón "Resetear" para dejar de recibir estas alertas**.


.. figure:: img/security/rkhunter.png
   :alt:

   Resetear Rkhunter.
