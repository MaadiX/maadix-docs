# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2020, MaadiX
# This file is distributed under the same license as the MaadiX Docs
# package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2021.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: MaadiX Docs release_202102\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-08-23 12:55+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.6.0\n"

#: ../../limesurvey.rst:2
msgid "LimeSurvey"
msgstr ""

#: ../../limesurvey.rst:4
msgid ""
"LimeSurvey es una aplicación de software libre y códico abierto que "
"permite la creación de encuestas o formularios en línea. Está orientada "
"tanto a principiantes como a profesionales, ya que facilita una "
"multiplicidad de tipos de respuesta, ramificación a partir de "
"condiciones, plantillas y diseño personalizado. Además, incluye "
"utilidades básicas de análisis estadístico para el tratamiento de los "
"resultados obtenidos."
msgstr ""
"Free and open software application for implementing online surveys "
"oriented to both beginners and professionals. It allows a multiplicity of"
" response types, branching from conditions, templates and custom design. "
"It also includes basic statistical analysis utilities for processing the "
"results obtained."

#: ../../limesurvey.rst:6
msgid ""
"Su guía de uso oficial la podrás encontrar  `aquí "
"<https://manual.limesurvey.org/LimeSurvey_Manual>`__."
msgstr ""
"The official user's guide can be found `here "
"<https://manual.limesurvey.org/LimeSurvey_Manual>`__."

#: ../../limesurvey.rst:9
msgid "Instalación"
msgstr "Installation"

#: ../../limesurvey.rst:11
msgid ""
"Desde el Panel de Control, ve al apartado '**Instalar Aplicaciones**', "
"ahí podrás ver todas las aplicaciones disponibles para instalar, entre "
"ellas LimeSurvey. Solo tienes que marcar la casilla *'Seleccionar'* e "
"indicar:"
msgstr ""
"From the Control Panel, go to the 'Install Applications' section, there "
"you will see all the applications available to install, including "
"LimeSurvey. Just check the 'Select' box and indicate:"

#: ../../limesurvey.rst:13
msgid ""
"Un **dominio o subdominio** donde lo quieres instalar. Este dominio tiene"
" que estar apuntando a la dirección IP del servidor, para ello tendrás "
"que añadir un registro A en el editor de zonas DNS de tu dominio "
"(operación externa a MaadiX). Recuerda que **este dominio o subdominio no"
" lo tienes que añadir en la sección 'Dominios'**."
msgstr ""
"A **domain or subdomain** where you want to install it. This domain has "
"to be pointing to the IP address of the server, for this you will have to"
" add an A record in the DNS zone editor of your domain (external "
"operation to MaadiX). Remember that **this domain or subdomain does not "
"have to be added in the 'Domains' section**."

#: ../../limesurvey.rst:14
msgid ""
"Una **contraseña** para asignarle a la cuenta de administración de "
"LimeSurvey. Guárdala en algún lugar seguro ya que no será enviada por "
"correo por razones de seguridad."
msgstr ""
"A **password** to set for the LimeSurvey administration account. Keep it "
"somewhere safe as it will not be mailed for security reasons."

#: ../../limesurvey.rst:16
msgid ""
"Una vez indicados estos dos parámetros puedes darle al botón de "
"**'Instalar'**."
msgstr ""
"Once you have specified these two parameters you can click on the "
"**'Install'** button."

#: ../../limesurvey.rst:21
msgid "Instalación de LimeSurvey."
msgstr "LimeSurvey installation."

#: ../../limesurvey.rst:23
msgid ""
"Después de unos minutos te llegará un email (a la dirección que tengas "
"configurada para la cuenta administración del Panel de Control) "
"confirmando que la instalación terminó correctamente."
msgstr ""
"After a few minutes you will receive an email (to the address you have "
"configured for the Control Panel administration account) confirming that "
"the installation was successfully completed."

#: ../../limesurvey.rst:26
msgid "Acceso"
msgstr "Access"

#: ../../limesurvey.rst:28
msgid ""
"Para acceder a LimeSurvey tendrás que ir al dominio o subdominio que "
"indicaste durante la instalación. También puedes acceder a través del "
"Panel de Control, en '**Mis Aplicaciones**' > '**LimeSurvey**' > '**Ir a "
"la aplicación**'."
msgstr ""
"To access LimeSurvey you will need to go to the domain or subdomain you "
"specified during installation. You can also access it through the Control"
" Panel, under '**My Applications'** > '**LimeSurvey**' > '**Go to "
"application**'."

#: ../../limesurvey.rst:30
#, fuzzy
msgid ""
"Podrás hacer *login* en el panel de administración añadiendo ``/admin`` "
"al final del dominio (por ejemplo: `lime.example.com/admin`) y acceder "
"con la contraseña que tu mismx asignaste durante el proceso de "
"instalación, por razones de seguridad no se envía por correo. **Si "
"olvidas la contraseña** puedes iniciar el proceso de recuperación de "
"contraseña haciendo **clic en '¿Contraseña olvidada?'** e indicando el "
"correo electrónico de la cuenta de administración."
msgstr ""
"You can login to the administration account by adding ``/admin`` to the "
"end of the domain (for example: `lime.example.com/admin`) and access with"
" the password you assigned yourself during the installation process, for "
"security reasons it is not sent by mail. If you forget the password you "
"can start the password recovery process by clicking on 'Forgot password' "
"and entering the email address of the administration account."

#: ../../limesurvey.rst:32
msgid ""
"Para que el proceso de recuperación de contraseña funcione debes "
"configurar previamente el envío de emails como se indica más abajo. Te "
"recomendamos hacerlo lo antes posible."
msgstr "In order to make the password recovery process work, you must first configure the email sending as indicated below. We recommend you to do it as soon as possible."

#: ../../limesurvey.rst:37
msgid "Login en LimeSurvey."
msgstr "LimeSurvey login."

#: ../../limesurvey.rst:41
msgid "Cambio de dominio"
msgstr "Domain change"

#: ../../limesurvey.rst:43
msgid ""
"Si necesitas cambiar el dominio o subdominio de LimeSurvey solo tienes "
"que ir al Panel de Control y en '**Mis aplicaciones**' > '**LimeSurvey**'"
" > '**Configurar**' para indicar el nuevo dominio. Este dominio tiene que"
" estar apuntando a la dirección IP del servidor, para ello tendrás que "
"añadir un registro A en el editor de zonas DNS de tu dominio (operación "
"externa a MaadiX). Recuerda que **este dominio o subdominio no lo tienes "
"que añadir en la sección 'Dominios'**."
msgstr ""
"If you need to change the domain or subdomain of LimeSurvey just go to "
"the Control Panel and in '**My applications**' > '**LimeSurvey**' > "
"'**Configure**' to indicate the new domain. This domain has to be "
"pointing to the IP address of the server, for this you will have to add "
"an A record in the DNS zone editor of your domain (external operation to "
"MaadiX). Remember that **this domain or subdomain does not have to be "
"added in the 'Domains' section**."

#: ../../limesurvey.rst:48
msgid "Cambio de dominio de LimeSurvey."
msgstr "LimeSurvey domain change."

#: ../../limesurvey.rst:52
msgid "Configuración de emails"
msgstr "Emails configuration."

#: ../../limesurvey.rst:54
msgid ""
"La configuración del envío de email se encuentra en el panel de "
"administración de LimeSurvey. Para acceder debes ir a al dominio de la "
"instalación y añadir ``/admin``, por ejemplo `lime.midominio.com/admin`."
msgstr "The email sending configuration is located in the LimeSurvey administration panel. To access it you must go to the installation domain and add ``/admin``, for example `lime.mydomain.com/admin`."

#: ../../limesurvey.rst:56
msgid ""
"Para configurar los parametros de envío de emails tienes que ir a "
"**\"Configuración\" > \"Global\" > \"Configuración de correo "
"electrónico\"**. Allí puedes marcar la opción SMTP e indicar los "
"parámetros siguientes:"
msgstr "To setup the parameters for sending emails you have to go to **"Configuration" > "Global" > "Email settings"**. There you can check the SMTP option and specify the following settings:"

#: ../../limesurvey.rst:58
msgid ""
"Servidor SMTP: el nombre de tu servidor seguido de :465 "
"(tuservidor.maadix.org:465)."
msgstr " SMTP host: your server name followed by :465 (yourserver.maadix.org:465)."

#: ../../limesurvey.rst:59
msgid ""
"Nombre de usuario para la autenticación en el SMTP: una cuenta de correo "
"de tu servidor (contacto@midominio.com)."
msgstr "SMTP username: an email account on your server (contact@mydomain.com)."

#: ../../limesurvey.rst:60
msgid ""
"Contraseña para la autenticación en el SMTP: contraseña de la cuenta de "
"correo anterior."
msgstr "SMTP password: password of the previous email account."

#: ../../limesurvey.rst:61
msgid "Cifrado SMTP: SSL/TLS."
msgstr "SMTP encryption: SSL/TLS."

#: ../../limesurvey.rst:66
msgid "Configuración de envío de emails."
msgstr "Email sender settings."

#: ../../limesurvey.rst:68
msgid ""
"Además tendrás que indicar una cuenta de correo como remitente válida en "
"**\"Configuración\" > \"Global\" > \"Configuración de rebotes de correo "
"electrónico\"**."
msgstr "You will also have to indicate a valid email account as sender in "Configuration" > "Global" > "Bounce settings"."

#: ../../limesurvey.rst:73
msgid "Configuración de remitente de emails."
msgstr "Bounce settings."
