Notificaciones
================

En la sección "Notificaciones" podrás gestionar lo relativo a las correos que se envían desde tu servidor para informarte de su estado, así como los correos que se envían a usuarixs con instrucciones de configuración.

Remitente de emails del sistema
---------------------------------

Si lo deseas puedes seleccionar del desplegable un correo remitente para cuando el sistema necesite enviar un correo a unx usuarix, por ejemplo con las instrucciones de una cuenta VPN.

Aclaración: este no será el correo remitente para el envío de logs.

.. figure:: img/notifications/sender.png
   :alt:

   Selección de remitente.


Destinatarix de logs
---------------------

El sistema envía periódicamente por correo electrónico una serie de logs informado del estado del servidor. Puedes elegir quién quieres que reciba estos emails: el equipo técnico de MaadiX, tu o ambxs. Puedes indicar el correo electrónico al que quieres que se te envíen los logs, si no se indica ninguno se usará el asociado a la cuenta de administración del Panel de Control.

Por defecto lo logs se enviarán al equipo técnico de MaadiX.

.. figure:: img/notifications/logs.png
   :alt:

   Selección de destinatarix de logs.


Los principales logs que se reciben son:

- Notificaciones de Puppet: cada vez que puppet se ejecuta o si experimenta un error.
- Notificaciones de monit: cada vez que hay un alto consumo de memoria RAM, CPU o espacio en disco y cada vez que monit se detiene por algún motivo.
- Notificaciones de logwatch: cada 24h se envía un resumen de los logs del sistema.
- Notificaciones de Security Events: cada 24h se envían los logs relativos a la seguridad del sistema.
- Notificaciones de Unattended Updates: se notificará cada vez que se detecte que algún paquete del sistema puede ser actualizado. No será necesario hacer nada, la herramienta Unattended Updates llevará a cabo las actualizaciones automáticamente (cada 7 días).

El correo remitente será monit@miserver.maadix.org (para las notificaciones de monit) o 
root@miserver.maadix.org (para el resto de notificaciones).
