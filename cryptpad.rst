Cryptpad
========
Cryptad es una aplicación que permite la edición colaborativa de documentos en tiempo real.
Se trata de una suite ofimática completa con todas las herramientas necesarias para una colaboración productiva. Las aplicaciones incluyen: Texto enriquecido,  Hojas de cálculo, Código/Markdown, Kanban, Diapositivas, Pizarra y Formularios. 
Puedes conocer más detalles en su web oficial: https://cryptpad.org/
   

Cryptad  añade una capa extra de seguridad ya que implementa cifrado de extremo a extremo.  Los datos de CryptPad se cifran en el navegador así que  ningún dato legible sale del dispositivo ni tampoco se almacena en el servidor.  Esto sí, si pierdes tu contraseña nadie podrá ayudarte a recuperarla y no podrás ya descifrar tus datos.

Instalación
------------

Accede al Panel de Control: en el apartado '**Instalar Aplicaciones**', podrás ver todas las aplicaciones disponibles para instalar, entre ellas Cryptpad. Solo tienes que marcar la casilla *'Seleccionar'* y rellenar los campos que aparecerán.

.. figure:: img/cryptpad/install.png
   :alt:

   Instalar Cryptpad.

Para instalar Cryptpad se necesitan dos dominios:  

- Un **dominio o subdominio** para la instalación. Se trata del dominio pronicpal que se utilizará para acceder a la aplicación y qye será visible en el navegador.

- Un **dominio o subdominio "sandbox"** que la aplicación necesita para su sitema de sandboxing, cuyo objetivo es reforzar la seguridad limitando el riesgo de vulnerabilidades de Cross-Site Scripting (XSS).  Los cálculos sensibles (como el procesamiento de claves criptográficas) se gestionan en el dominio principal, mientras que la interfaz de usuario se implementa en el dominio sandbox.

Ambos dominios tienen que estar apuntando a la dirección IP del servidor. Para ello tendrás que añadir dos registro A en el editor de zonas DNS de tu dominio (operación externa a MaadiX). 
Puedes consultar esta página para más información sobre como `crear las entradas DNS </dns/#registro-de-tipo-a>`__ necesarias.


Además de los dos dominios puedes elegir activar los dos siguientes parámetros, que en todo caso podrás volver a modificar en cualquier momento desde el panel de control :


- **Acceso sin Registro**: si activas esta casilla permitirás el uso de CryptPad sin necesidad de que las personas estén registradas.
- **Activar limpieza automática**: activando esta opción se habilitará la eliminación definitiva y automática de los archivos en papelera. La papelera en Cryptad se llena de forma automática de la siguiente forma:  

  - Documentos que no pertenecen a ninguna cuenta registrada: se eliminarán después de 90 días.
  - Elementos eliminado manualmente. Se borrarán definitivamente después de 15 días.
  - Cuentas inactivas y sus documentos: se eliminarán después de 365 días de inactividada.

Una vez establecidos los parámetros desdeados  puedes hacer clic en el botón **'Instalar'**.

Crear cuenta admin
------------------

Después de unos minutos, cuado se complete la instalación, te llegará un email de confrmación en el que encontrarás el enlace que te permitirá crear la cuenta de administración de Cryptpad.

La primera cuenta que se registre uilizando este enlace será la cuenta con poderes de administración, así que te recomendamos completar el registro en cuanto recibas el correo electrónico.

Su contraseña es la clave secreta que cifra todos los documentos así como los privilegios de administración de la instancia. Si se pierde, **nadie podrá recuperar los datos**.

.. figure:: img/cryptpad/warning.png
   :alt:

   Contraseña  Cryptpad.

El aviso que sale en el momento de activar la cuenta es cierto. Tampoco el equipo de MaadiX podrá ayudarte a recuperar el acceso a tus documetnos en el caso de que pierdas la contraseña.

Aquí puedes consultar la documentación  oficial para la administración de Cryptpad desde la interfaz gráfica y las opciones que ofrece:
https://docs.cryptpad.org/en/admin_guide/index.html


Cambio de dominio y parámteros
------------------------------

Si necesitas cambiar los dominios o subdominios Cryptpad tienes que ir al Panel de Control y en '**Mis aplicaciones**' > '**Cryptpad**' > '**Configurar**'  indicar  los nuevos dominios. 
Desde esta misma pagina podrás modifcar los valores para peritir el acceso sin registro o para activar el vacíado automático de la papelera.
Los demás paramteros los podrñas confgurar directamente desde la interfaz de administración de Cryptpad. 


.. figure:: img/cryptpad/config.png
   :alt:

   Cambio de dominio de Cryptpad.
