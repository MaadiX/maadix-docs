Copias de seguridad
===================

Desde la release 202301 de MaadiX será disponible la opción de contratar el servicio de 
copias de seguridad que se ofrece utilizando el software **Borg**. Las copias se 
almacenan en un servidor remoto y son cifradas en el origen. 

El servidor remoto no conoce la clave necesaria para descifrar las copias. 

La herramienta Borg Backup crea copias de seguridad utilizando la técnica de «deduplicating». 
Esta técnica permite crear copias de forma incremental, es decir, sólo los nuevos archivos o 
los que han sido modificados son almacenados en las nuevas copias, lo que permite aprovechar 
al máximo los recursos.

Para recuperar las copias creadas por Borg y navegar entre las carpetas es necesario primero 
**descencriptarlas** y luego transformar los archivos incrementales  creados por Borg en archivos 
legibles que permitan acceder a su contenido.

Puedes consultar la página oficial del proyecto aquí:  https://www.borgbackup.org/

El Panel de Control ofrece ambas funcionalidades, pero es imprescindible hacer una copia de respaldo 
de la clave necesaria para desencriptar los backups y guardarla en uno o más dispositivos externos.
Sin esta copia, en el caso de que tu sistema se viera comprometido, los backups serían ilegibles.

El equipo de MaadiX no conoce esta clave secreta. así que si la pierdes, no habrá forma de 
acceder al contenido de las copias de seguridad.

Copiar Clave
------------

Una vez activado el servicio de copias de seguridad recibirás un correo en el que se te 
recomienda acceder al Panel de Control para bajarte una copia de la clave de encriptación. 
La clave de encriptación es un *'secreto'* en formato de texto que puedes guardar en un archivo.
Hasta que no completes este proceso no podrás efectuar ninguna otra tarea desde el panel de control. 

La primera vez que accedas al panel de control después de haber activado el sistema de backup 
verás una alerta como esta:

.. figure:: img/borg/alert-key-not-copied.png
   :alt: Add pgp key 

        
El acceso a otras secciones del panel de control estará inhibido hasta 
que completes el proceso de exportación de la clave.

El primer paso es solicitar la exportación de la clave, haciendo clic en 'Exportar Clave'

.. figure:: img/borg/export-key.png
   :alt: Export pgp key 

El proceso de exportación puede tardar entre uno y tres minutos. 
Espera hasta que aparezca la siguiente ventana


.. figure:: img/borg/exporting-key.png
   :alt: Exporting pgp key 


Una vez la clave esté exportada se mostrará un campo en le que tendrás que insertar 
la contraseña de acceso al panel de control para poderla visualizar

.. figure:: img/borg/insert-pass.png
   :alt: Exporting pgp key 

Puedes utilizar el botón 'Copiar Clave' para asegurarte no cometer errores en 
la selección de la cadena de caracteres

.. figure:: img/borg/copy-key.png
   :alt: Copy pgp key

Una vez copiada la cadena pégala en una archivo de texto plano (.txt) y guárdala en uno 
o más dispositivos externos seguros (ordenador, disco duro, etc).
No olvides hace clic en el botón 'Clave guardada' para que el panel de control 
no vuelva a obligarte a repetir el proceso.

.. figure:: img/borg/save-key.png
   :alt: Saved pgp key

Podrás volver a hacer una copia de la clave en cualquier momento visitando la página 
'Sistema' -> 'Copias de seguridad' 


Acceder a las copias
--------------------

Puedes consultar las copias de seguridad disponibles desde la pestaña 'Sistema' -> 'Copias de seguridad'.
Está página muestra un listado de las copias existentes y te permite 'Montar' o ''Desmontar' cualquiera de ellas.

.. figure:: img/borg/mount-umount.en.png
   :alt: Copy pgp key


Si una copia está montada puedes acceder a ella accediendo por SSH/SFTP con la cuenta Supeusuarix 
y tendrás disponibles todos los archivos que contiene para copiarlos o descargarlos. 
El acceso a copias montadas es de solo lectura. No se pueden modificar.

Las copias montadas no ocupan espacio en el disco del servidor. Se trata de un punto de montaje 
que reconstruye el estado de los archivos en una fecha determinada.

Si reinicias el servidor el punto de montaje se desactiva. Tendrás entonces que volver a 
montarlo desde el panel de control si quieres acceder a los archivos.

El ciclo que determina cuántas copias se conservan está especificado en la columna derecha.

Cuando una copia es borrada por el ciclo de rotación, ya no saldrá en el listado de copias 
disponibles y no se podrá volver a montar.

Sin embrago, si ya estaba montada en el momento de ser borrada por el ciclo de rotación, 
segurá disponible y podrás eseguir accediendo a los archivos.
Puedes diferenciar estas copias porque se listan debajo de las anteriores como
**Carpetas Montadas en Caché** como puedes ver en la imagen anterior.

Hay que tener en cuenta que las carpetas montadas se desmontan en cada reinicio del servidor 
y además utilizan recursos del sistema, así que la mejor práctica para conservar copias antiguas 
es descargarlas en otro dispositivos.

¿Qué copias se hacen?
---------------------

Se hace copia de todas las bases de datos mysql con un único dump.
En el caso de tener instaladas aplicaciones que utilizan MongoDB o PostgreSQL también se hace backup de sus bases de datos.

También se hace copia del directorio  openLdap que contiene toda la información añadida vía panel de control (aplicaciones instaladas, cuentas ordinarias y de correo electrónico, dominios , configuraciones del servidor, etc).

En cuanto a archivos, se hace copia del contenido de todos los siguiente directorios:

::

    /boot →  Contiene todo lo necesario para el proceso de arranque
    /etc → Contiene archivos de configuración del sistema y de varios servicios (Let’s encrypt, Vhosts de los dominios, Mysql, SSH, Apache...)
    /home  → Contiene las carpetas de las cuentas en /home/username y los crreos en /home/vmail
    /opt  → Contiene archivos de configuración y la aplicación Mailman si está instalada
    /root → Contiene archivos de configuración de root
    /usr → Contiene librerías y aplicaciones, entre ellas el panel de control
    /srv  → Contiene datos específicos de algunas aplicaciones. De momento en MaadiX no se utiliza
    /var → Contiene aplicaciones y las carpetas de los dominios crreados desde el panel de control (webs, Nextcloud...)
 
De estas se excluyen algunas subcarpetas para evitar redundancias y archivos temporales


``/home/.trash  → Contiene los archivos que se hayan borrado desde el panel de control``

 Todas las siguientes carpetas son librerías, archivos de cache o binarios que no tiene sentido respaldar y se excluyen del backup:

::
    
    /usr/bin
    /usr/lib
    /usr/sbin
    /usr/src
    /var/backups
    /var/cache
    /var/lib/apt
    /var/lib/docker
    /var/lib/dpkg
    /var/lib/fail2ban
    /var/lib/mlocate
    /var/lib/mongodb
    /var/lib/munin
    /var/lib/mysql
    /var/lib/postgresql
    /var/lib/smartmontools


Se aplica un ciclo de rotación que conserva las siguientes copias:
7 diarias (los últimos 7 días) 8 semanales, 4 mensuales.

Puedes consultar el script aquí:

 https://gitlab.com/MaadiX/maadix-modules/-/blob/release-202301/modules-custom/helpers/templates/borgbackup.sh
