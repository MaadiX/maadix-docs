Nextcloud
=========

Nextcloud es un software (libre y de código abierto) que te permite
crear una nube segura donde almacenar tus archivos. Es una alternativa a
servicios como Dropbox o Google Drive.

Además, también se le pueden añadir aplicaciones de calendario, gestión
de tareas, gestión de contactos, videollamadas, etc.

Instalación
-----------

Desde el Panel de Control, ve al apartado '**Instalar Aplicaciones**',
ahí podrás ver todas las aplicaciones disponibles para instalar, entre
ellas Nextcloud. Solo tienes que marcar la casilla *'Seleccionar'* e indicar:

- Un **dominio o subdominio** donde lo quieres instalar. Este dominio tiene que estar apuntando a la dirección IP del servidor, para ello tendrás que añadir un registro A en el editor de zonas DNS de tu dominio (operación externa a MaadiX). Recuerda que **este dominio o subdominio no lo tienes que añadir en la sección 'Dominios'**.
- Una **contraseña** para asignarle a la cuenta de administración de Nextcloud. Guárdala en algún lugar seguro ya que no será enviada por correo por razones de seguridad.

Una vez indicados estos dos parámetros puedes darle al botón de **'Instalar'**.

Después de unos minutos en los que se hace la instalación ya tendrás tu nube segura con Nextcloud instalada en Maadix. Te llegará un email (a la dirección que tengas configurada para la cuenta administración del Panel de Control) confirmando que la instalación terminó correctamente.

Acceso
------

Para acceder a Nextcloud tendrás que ir al dominio o subdominio que indicaste durante la instalación. También puedes acceder a través del Panel de Control, en '**Mis Aplicaciones**' > '**Nextcloud**' > '**Ir a la aplicación**'.

Podrás hacer *login* con la contraseña que tu mismx asignaste durante el proceso de instalación, por razones de seguridad no se envía por correo. **Si olvidas la contraseña** puedes iniciar el proceso de recuperación de contraseña haciendo **clic en '¿Contraseña olvidada?'** e indicando el correo electrónico de la cuenta de administración (si no lo has cambiado será el mismo que el de la cuenta de administración del Panel de Control).

.. figure:: img/nextcloud/nc-acceso2.png
   :alt:

   Login en Nextcloud.


Cambio de dominio
------------------

Si necesitas cambiar el dominio o subdominio de Nextcloud solo tienes que ir al Panel de Control y en '**Mis aplicaciones**' > '**Nextcloud**' > '**Configurar**' para indicar el nuevo dominio. Este dominio tiene que estar apuntando a la dirección IP del servidor, para ello tendrás que añadir un registro A en el editor de zonas DNS de tu dominio (operación externa a MaadiX). Recuerda que **este dominio o subdominio no lo tienes que añadir en la sección 'Dominios'**.

.. figure:: img/nextcloud/dominio.png
   :alt:

   Cambio de dominio de Nextcloud.


Actualizaciones
---------------

Las actualizaciones de Nextcloud se harán de forma automática cuando
hagas la actualización a una nueva release de MaadiX (ver documentación
sobre `actualizaciones </updates>`__).

Sin embargo, las actualizaciones de las aplicaciones que instales en
Nextcloud las tendrás que hacer desde Nextcloud: '**Aplicaciones**' >
'**Aplicaciones Activas**' y allí darle al botón '**Actualizar**' en las
aplicaciones que hayan sacado alguna actualización.
