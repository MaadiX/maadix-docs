Zeyple PGP
===================

Esta aplicación permite cifrar los correos que se envían desde el servidor.
En el caso de que el sistema disponga de la clave pública pgp de una cuenta de correo, 
Zeyple añade automáticamente la encriptación para los mensajes enviados a esta cuenta. 
Las claves públicas se pueden añadir desde el Panel de Control  una vez instalado Zeyple.


¿Qué correos se cifran?
-----------------------

Hay dos tipos de notificaciones:
    
- Desde el panel de Control:
    Recuperación de contraseña para el Panel de Control  
    Envío de instrucciones para configurar el cliente VPN   
- Logs de sistema:
              
    Periódicamente el sistema envía correos electrónicos que incluyen información sobre 
    el estado del mismo (logs). Puedes consultar más detalles sobre qué tipo de correos 
    envía el sistema en la sección `Notificaciones  </notifications>`__

De momento Zeyple no puede encriptar los correos sobre el estado del sistema (logs) a más de una cuenta. Esto significa que si activas Zeyple y en la sección 'Notificaciones' tienes seleccionada la opción de que los logs se envíen tanto al equipo de MaadiX como a otra cuenta, dichos correos se enviarán sin cifrar.
Actualmente la única opción que tienes para recibir los logs cifrado es seleccionar la opción 'Solo yo'.

Subir Claves
------------

Para que los correos que salen del sistema se puedan cifrar es necesario que añadas 
la clave pública PgP de las cuentas de destino desde el apartado 
'Mis Aplicaciones' -> 'Claves PGP' del panel de control.

Solo tendrás que copiar y pegar la calve pública y el sistema se encargará de registrar 
dicha clave y asociarla al correo electrónico correspondiente.

.. figure:: img/zeyple/add-pgp-key.png
   :alt: Add pgp key 

Errores
-------

Si una clave ha expirado Zeyple, en lugar de enviar el correo en texto plano, no lo entregará. 

Si experimentas problemas de entrega con correos cifrados, averigua que la clave que tienes 
guardada desde el panel de control sea válida y no haya expirado.

En caso contrario elimínala y vuelve a cargar la nueva clave.

Hay alguna discusión abierta sobre este comportamiento, por lo que es posible que 
en un futuro sea diferente:


https://github.com/infertux/zeyple/issues/20
https://github.com/infertux/zeyple/issues/31

Página oficial de Zeyple: https://infertux.com/labs/zeyple/
