PeerTube
========

PeerTube es una plataforma  descentralizada y de código abierto para compartir vídeos y realizar emisiones en directo y que, gracias a la federación con otras instancias, permite interactuar con otras comunidades.

Instalación
-----------

PeerTube puede requerir un considerable espacio en disco debido a su funcionamiento. Por ello, dependiendo de tu previsión de uso (número de cuentas, cantidad de vídeos, configuración de la instancia, etc...) recomendamos optar por un almacenamiento externo S3. Esta solución no solo es más económica, sino también más eficiente para gestionar grandes volúmenes de datos.

La instalación de PeerTube utilizando el almacenamiento SSD interno del servidor está deshabilitada por defecto. Esto se debe a que podría consumir rápidamente el disco interno, afectando al funcionamiento de otras aplicaciones. Esta medida busca evitar instalaciones que puedan comprometer el rendimiento del servidor sin que seas plenamente consciente de las implicaciones.

Aunque técnicamente es posible migrar los datos de SSD a S3 posteriormente, este proceso debe realizarse manualmente e implica un coste adicional. Si decides instalar PeerTube utilizando el almacenamiento interno, deberás solicitar la habilitación de esta opción manualmente. A continuación, te proporcionamos las instrucciones para llevar a cabo ambas configuraciones.

Accede a la sección **Aplicaciones** -> **Instalar Aplicaciones** del menú izquierdo del Panel de Control de tu Servidor.
En la lista de aplicaciones disponibles encontrarás PeerTube. Es posible que no tengas habilitada la casilla para seleccionarla, tal como aparece en la siguiente imagen.

.. figure:: img/peertube/peertube-can-not-install.png
      :alt: Instalación deshabilitada de PeerTube
      :align: center


      Instalación deshabilitada de PeerTube

En este caso debes decidir que tipo de instalación quieres: con S3 o con el dsico SSD interno del servidor

Usar  S3
~~~~~~~~

Para instalar PeerTube con almacenamiento externo S3, primero necesitas contratar este servicio desde tu área de cliente. Podrás añadir servicios adicionales para cualquiera de los servidores que tengas contratados. 


.. figure:: img/contratar-s3-server.png
         :alt: Añadir S3 
         :align: center
         :figwidth: 60%


         Añadir S3 al servidor


Al hacer clic en el icono correspondiente, tendrás la opción de elegir el plan S3 que mejor se adapte a tus necesidades, con la posibilidad de ajustarlo (aumentar o reducir) en cada renovación. 


.. figure:: img/peertube/contratar-s3-peertube.png
            :alt: Añadir S3 
            :align: center
            :figwidth: 60%


            Elegir plan S3 para PeerTube 

Una vez realizado el pedido, se procederá a activar el nuevo espacio de almacenamiento, un proceso que puede tardar desde unos minutos hasta unas horas.

Cuando recibas la confirmación por correo de que el almacenamiento S3 está habilitado y operativo, podrás continuar con la instalación de PeerTube desde el Panel de Control. Simplemente accede a la sección **Aplicaciones** -> **Instalar Aplicaciones** y selecciona PeerTube


.. figure:: img/peertube/peertube-install-with-s3.png
            :alt: Instalar PeerTube con S3 
            :align: center
            :figwidth: 60%


            Instalar PeerTube con S3


Durante el proceso de instalación, deberás rellenar los siguientes campos: 


  - **Dominio o subdominio para la aplicación**: Será la dirección principal para acceder a la interfaz web de PeerTube y será visible en el navegador.

  - Una **contraseña**:  Se asignará a la cuenta de administración de la aplicación. Guárdala en algún lugar seguro ya que no será enviada por correo por razones de seguridad.

  - **Dominio o subdominio para los medios**: Este se usará para servir imágenes, videos y otros medios almacenados en el espacio S3.

Ambos dominios tienen que estar apuntando a la dirección IP del servidor. Para ello tendrás que añadir dos registro A en el editor de zonas DNS de tu dominio (operación externa a MaadiX).
Puedes consultar esta página para más información sobre como `crear las entradas DNS </dns/#registro-de-tipo-a>`__ necesarias.

Tras configurar estos parámetros, haz clic en "Instalar".


Usar SSD
~~~~~~~~

Para instalar PeerTube de forma que utilice el almacenamiento de disco SSD interno, primero deberás solicitar que se habilite esta opción. Puedes hacerlo abriendo un ticket desde tu área de cliente o enviando un correo electrónico a la cuenta de soporte de MaadiX.

Una vez habilitada, podrás proceder con la instalación. Durante el proceso, será necesario indicar el dominio que se utilizará para acceder a la aplicación. Este dominio debe estar correctamente configurado para apuntar a la dirección IP del servidor.

Para ello, deberás añadir un registro A en el editor de zonas DNS de tu dominio, lo cual es una operación externa a MaadiX.


.. figure:: img/peertube/peertube-install-without-s3.png
         :alt: Instalar PeerTube con SSD
         :align: center
         :figwidth: 60%


         Instalar PeerTube con SSD


Una vez habilitada, podrás proceder con la instalación. Durante el proceso, será necesario indicar el dominio que se utilizará para acceder a la aplicación. Este dominio debe estar correctamente configurado para apuntar a la dirección IP del servidor.

Para ello, deberás añadir un registro A en el editor de zonas DNS de tu dominio, lo cual es una operación externa a MaadiX. Puedes consultar esta página para más información sobre como `crear las entradas DNS </dns/#registro-de-tipo-a>`__ necesarias.



Primer Acceso
-------------

Una vez completada la instalación, recibirás un correo electrónico con los datos necesarios para acceder a PeerTube. La instalación crea una cuenta de administración con privilegios para configurar la instancia y sus opciones. 


Configuración
-------------
Una vez completada la instalación, es muy recomendable añadir los registros DKIM y SPF a las entradas DNS del dominio para evitar que los correos enviados por la aplicación (notificaciones, recuperación de contraseña, etc.) terminen en la carpeta de no deseados o, en el peor de los casos, sean rechazados.

Desde el panel de control, accede a Mis Aplicaciones → PeerTube → Configurar. En la columna derecha encontrarás los valores que deberás asignar a los dos registros DNS.
En esta página puedes encontrar indicaciones específicas para la creación de las entradas DNS: `crear las entradas DNS </dns/#registro-spf>`__ para correo.

 En cuanto a la edición, el único parámetro configurable es el dominio para los medios en S3. Por el contrario, el dominio principal de la aplicación no se puede modificar posteriormente de forma segura, ya que actúa como el identificador único de tu servidor dentro de la red. Se trata de una operación delicada que debe realizarse con sumo cuidado y de forma manual.

Desde la interfaz gráfica de la aplicación podrás invitar personas a unirse a tu nueva instancia y editar los parámetros disponibles tales como políticas para el registro de nuevas cuentas, federaciones, etc...

En esta paǵina puedes consultar más detalles: 
https://docs.joinpeertube.org/









