2FA - Doble autenticación
=============================

La autenticación en dos pasos o "Two-Factor Authentication" (2FA) es un mecanismo de protección de acceso al Panel de Control. Como su nombre indica, consiste en la solicitud de una doble autenticación cada vez que accedas al Panel de Control. Para hacer uso de esta funcionalidad vas a necesitar una aplicación móvil de doble autenticación, por ejemplo:

- `FreeOTP  <https://freeotp.github.io/>`__: descarga en `F-droid <https://f-droid.org/packages/org.fedorahosted.freeotp/>`__; descarga en `Google Play <https://play.google.com/store/apps/details?id=org.fedorahosted.freeotp>`__; descarga en `App Store (iOS) <https://apps.apple.com/us/app/freeotp-authenticator/id872559395>`__.

- `andOTP <https://github.com/andOTP/andOTP>`__: descarga en `F-droid <https://f-droid.org/packages/org.shadowice.flocke.andotp/>`__; descarga en `Google Play <https://play.google.com/store/apps/details?id=org.shadowice.flocke.andotp>`__.

- `Aegis Authenticator <https://getaegis.app/>`__: descarga en `F-droid <https://f-droid.org/en/packages/com.beemdevelopment.aegis/>`__; descarga en `Google Play <https://play.google.com/store/apps/details?id=com.beemdevelopment.aegis>`__.

- `Tofu Authenticator <https://www.tofuauth.com/>`__: descarga en `App Store (iOS) <https://apps.apple.com/us/app/freeotp-authenticator/id872559395>`__.


Activación
----------

Para activar la Doble Autenticación tienes que ir al Panel de Control y en **"Sistema"** > **"2FA"** hacer clic en "**Activar Autenticación de Dos Pasos**". Entonces se te pedirá escanear un código QR (para ello necesitarás una de las aplicaciones mencionadas anteriormente) e indicar el código proporcionado por la app.

Recuerda que una vez activada la doble autenticación, el móvil utilizado quedará vinculado con tu cuenta y será el único que podrá recibir el código para acceder al Panel de Control. En caso de que pierdas el acceso a tu dispositivo, también podrás disponer de los "códigos de respaldo" para acceder.


.. figure:: img/2fa/qr.png
   :alt:

   Activación de 2FA.

Uso
-----

Una vez activada la funcionalidad 2FA, cada vez que quieras acceder al Panel de Control tendrás que:

- En primer lugar: introducir tus credenciales de acceso a Panel de Control (como hacías normalmente).
- En segundo lugar: se te pedirá un Token (número de 6 cifras) que te ofrecerá la aplicación móvil con la que has hecho la vinculación al dispositivo.

.. figure:: img/2fa/login.png
   :alt:

   Login en el Panel de Control.

Si no tienes acceso a este dispositivo podrás introducir un "Código de respaldo". Para obtener la copia de los "códigos de respaldo" hay que acceder al Panel de Control y en "**Sistema**" > "**2FA**", en la sección "Códigos de respaldo", hacer clic en "**Mostrar códigos**". Si todavía no tienes ningún código de respaldo haz clic en "Generar códigos de respaldo". Cada código es de un solo uso. Antes de utilizarlos todos no olvides generar un nuevo conjunto.

.. figure:: img/2fa/bk.png
   :alt:

   Códigos de respaldo.

Si no tienes códigos de respaldo ni acceso a el dispositivo vinculado, puedes proceder a la desactivación de 2FA por línea de comandos como se explica a continuación.


Desactivación
-------------------

Desde el Panel de Control
~~~~~~~~~~~~~~~~~~~~~~~~~~

Si en algún momento quieres deshabilitar la funcionalidad de doble autenticación puedes hacerlo desde el Panel de Control, en "**Sistema**" > "**2FA**", hacer clic en "**Deshabilitar la Autenticación de Dos Pasos**". Se te pedirá que confirmes tu decisión.

.. figure:: img/2fa/disable_2fa.png
  :alt:

  Desactivación de 2FA desde el Panel de Control.


Por línea de comandos
~~~~~~~~~~~~~~~~~~~~~~~~~~

Si no tienes acceso al Panel de Control tendrás que acceder al servidor por ssh con la cuenta Superusuarix y ejecutar los siguientes comandos:

1. Para acceder por ssh: ``ssh superusuarix@miservidor.maadix.org``

2. Obtener permisos de Superusuarix: ``sudo su``

3. Loguearse con usuarix del Panel de Control: ``sudo -u mxcp -s``

4. Ir al directorio del Panel de Control: ``cd /usr/share/mxcp/``

5. Activar entorno virtual: ``source venv3/bin/activate``

6. Ir al directorio donde está el archivo que se quiere ejecutar: ``cd maadix-cpanel``

7. Ejecutar el comando para la desactivación de 2FA (sustituir USER_CPANEL por usuarix del panel de control): ``./manage.py two_factor_disable USER_CPANEL``

8. Desactivar el entorno virtual: ``deactivate``

9. Desloguearse: ``exit``
