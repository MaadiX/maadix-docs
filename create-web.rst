Aloja tu web
============

Aquí tienes un resumen de los pasos a seguir para alojar una página o aplicación web en un
servidor de MaadiX:

1. En el Panel de Control de MaadiX, crear una cuenta ordinaria con acceso SFTP o SSH para que sea la *webmaster* de la web.
2. En el Panel de Control de MaadiX, añadir el dominio o subdomino y asignarle como *webmaster* la cuenta creada anteriormente.
3. Conectarse al servidor con la cuenta *webmaster* para subir los ficheros de la web al directorio ``/var/www/html/ejemplo.com``.
4. En el editor de zonas DNS de tu proveedor de dominio, añadir un registro A que apunte el dominio o subdominio a la IP del servidor (**operación externa a MaadiX**).
5. Esperar hasta que el servidor detecta que los DNS están apuntando al servidor (puede tardar unos minutos).
6. En el Panel de Control de MaadiX, activar el servidor web para el dominio: "Editar dominio" > "Activar servidor web" (tarda unos segundos en activarse).

Más abajo puedes encontrar una explicación más detallada de cada paso. El orden de los pasos se puede variar según necesidades.

Recuerda que **podrás alojar tantas webs como desees**, no hay límite de
dominios, solo tienes que contar con suficiente espacio y recursos en el
servidor.

La cuenta webmaster de tu web
-----------------------------

Lo primero que necesitas para gestionar una web en un servidor de MaadiX
es una cuenta ordinaria con acceso por SSH o SFTP que haga de
*webmaster*.

Recuerda que la cuenta **Superusuarix no podrá ser asignada como
webmaster**.

Esta funcionalidad está pensada para que la persona que hace el
desarrollo y actualizaciones de una web tenga su propia cuenta y
permisos limitados, solo los necesarios para subir los archivos que
conforman la web. No podrá hacer otras modificaciones en el servidor.

Esto es útil para organizaciones en las que la persona que administra el
servidor es diferente a la que hace el desarrollo de la web.

Para crear una cuenta ordinaria tendrás que ir a el panel de control, a
la sección 'Usuarixs'> 'Cuentas ordinarias' y darle al botón "Crear
nueva cuenta".

**Para que una cuenta ordinaria pueda ser asignada como webmaster tiene
que tener permisos de acceso por SFTP o SSH**. Puedes elegirlo en el
momento de creación de la cuenta en el apartado "Acceso SSH o SFTP al
servidor".

Una misma cuenta podrá ser asignada como *webmaster* de varios dominios.

En el caso de que tu web o aplicación web haga uso de bases de datos
(como es el caso de Wordpress), probablemente sea útil que le actives a
esta cuenta el acceso a phpMyAdmin (puedes ver más detalles
`aquí </mysql>`__).


Añadir el dominio a MaadiX
---------------------------

Para añadir tu dominio o subdominio tendrás que ir al Panel del Control, al apartado 'Dominios' > 'Añadir un dominio nuevo'.

En este momento tienes que asignar como *webmaster* la cuenta ordinaria creada anteriormente, selecciónala del desplegable. Puedes editarla posteriormente si lo necesitas.

.. figure:: img/CrearWeb/webmaster.png
  :alt: Screenshot

  Asignar webmaster al dominio.


En `esta documentación </dominios>`__ puedes encontrar los detalles de como se gestionan los dominios en MaadiX.

Cuando se añade el dominio el servidor comprueba si este dominio está apuntando a su IP, si es correcto, se activará el servidor web y verás en el Panel de Control "Servidor web: Activado". En el caso de que el domino aún no esté apuntando al servidor, el servidor web no se activará. Posteriormente se podrá activar en "Editar dominio" > marcar la casilla "Activar servidor web" > "Guardar".

.. figure:: img/CrearWeb/ServidorWeb.png
  :alt: Screenshot

  Detalles del dominio añadido.

Además, en el momento en el que se añade un dominio en el Panel de Control, en el servidor se crea el directorio ``/var/www/html/ejemplo.com``, aunque aún el servidor web esté desactivado (es decir, aún no se ha creado el virtual host de Apache ni los certificados de Let's Encrypt). Esto es útil para planificar migraciones e ir subiendo los archivos que conforman la web antes de cambiar los DNS.

**Nota**: en el caso de que no tengas un dominio propio puedes usar *subdominio.maadix.org*, donde 'subdominio' coincide con el nombre que elegiste al adquirir tu servidor MaadiX. En este caso, tienes que subir los archivos de la web a ``/var/www/html/subdominio.maadix.org`` con la cuenta Superusuarix.

Subir contenidos
----------------

Una vez hayas añadido el dominio, tendrás que subir los archivos de tu
web o aplicación al directorio ``/var/www/html/ejemplo.com``.

Puedes acceder a esta ubicación con la cuenta asignada como *webmaster* y
subir los archivos utilizando un cliente SFTP. En caso de que no tengas
ningún cliente SFTP instalado y no supieras cual escoger,
`Filezilla <https://filezilla-project.org/>`__ es uno de los más usados
y sencillos.

Recuerda que, por una cuestión de seguridad, en en los servidores MaadiX
**no están permitidas las conexiones por FTP**, solo por SFTP.


Con una cuenta SFTP
~~~~~~~~~~~~~~~~~~~~

Este será el caso en el que hayas asignado como *webmaster* una cuenta que
solo tiene acceso por SFTP. Las cuentas SFTP solo tendrán acceso por
SFTP y no por terminal (por SSH).

Esta cuenta será la propietaria de la carpeta
``/var/www/html/ejemplo.com``, de manera que sólo conectando con esta
cuenta podrás modificar los archivos que se encuentran en ella.

Las credenciales para la conexión haciendo uso de Filezilla (u otro
cliente SFTP), serán las siguientes:

-  **Servidor**: sftp://subdomino.maadix.org
-  **Protocolo**: SFTP (recuerda especificarlo).
-  **Nombre de la cuenta**: la cuenta *webmaster*.
-  **Contraseña**: la contraseña de la cuenta *webmaster*.
-  **Puerto**: por defecto será el puerto 22 (ver más en el apartado
   `seguridad </security>`__).

.. figure:: img/CrearWeb/SftpWeb.png
   :alt: Screenshot
   :target: ../_images/SftpWeb.png

   Cliente SFTP (acceso con cuenta SFTP).

Cuando se establezca la conexión desde una cuenta SFTP verás una carpeta
con su nombre. En ella, encontrarás una carpeta con el nombre del
dominio o subdominio (que es un enlace simbólico a
``/var/www/html/ejemplo.com``). Podrán haber varias carpetas si esta
cuenta es *webmaster* de otros dominios.

También puede tener ahí otros archivos propios que haya subido o creado
anteriormente. Las cuentas *webmaster* sólo tienen acceso a esta zona y no
a todos los archivos del sistema (como sí tiene la cuenta Superusuarix).

En la carpeta con el nombre del dominio ``/Webmaster/ejemplo.com`` es
donde se tendrán que subir los ficheros que conforman la web o
aplicación web. Se hará fácilmente arrastrando los archivos desde el
equipo local.

**Nota**: si haces demasiados intentos de conexión fallidos (más de 6)
con un cliente SFTP el servidor bloqueará la IP de tu conexión (de tu
oficina o vivienda). Encuentra más información `aquí </fail2ban>`__.

Con una cuenta SSH
~~~~~~~~~~~~~~~~~~

Este será el caso en el que hayas asignado como *webmaster* una cuenta que
tiene acceso por SFTP y por SSH. Esta cuenta sí podrá acceder al
servidor tanto por terminal (por SSH) como como utilizando un cliente
SFTP.

La forma de acceder por SFTP es parecida a la descrita en el apartado
anterior, pero en este caso la ubicación en la que aterrizas cuando se
establece la conexión es diferente. Estas cuentas disponen de un
directorio propio en ``/home/USERNAME`` y al contrario de las cuentas
SFTP no están enjauladas en él. Pueden acceder a otros directorios y
efectuar operaciones de lectura/escritura sobre aquellos archivos de los
que sean propietarias.

Tendrás que navegar a través de las carpetas hasta llegar a
``/var/www/html/ejemplo.com/``, que es donde tendrás que subir o crear
los archivos de tu nueva web o aplicación. Otra opción más rápida es
escribir la ruta ``/var/www/html/ejemplo.com/`` en el campo "Sitio
remoto".

Una vez ahí, puedes seguir los mismos pasos descritos para las cuentas
SFTP.

.. figure:: img/CrearWeb/SshWeb.png
   :alt: Screenshot
   :target: ../_images/SshWeb.png

   Cliente SFTP (acceso con cuenta SSH).


Apuntar el dominio al servidor
------------------------------

Para poder crear un sitio web accesible desde cualquier navegador
necesitarás un dominio o subdominio que tendrás que registrar con algún
proveedor (por ejemplo, `gandi.net <https://www.gandi.net/>`__ o
`Njal.la <https://njal.la/>`__, en Internet podrás encontrar muchos más).

Una vez ya tienes tu dominio, puedes ir a su editor de zonas DNS y
añadir un registro A que apunte a la IP de tu servidor (la IP de tu
servidor la puedes ver en el Panel de Control > Dominios >
Instrucciones).

Recuerda que **esta es una operación externa a MaadiX** y que cada
proveedor y editor de zona DNS es diferente. **MaadiX no es un proveedor
de dominios ni ofrece un editor de zonas DNS.**

Normalmente el proveedor con el que registras tu dominio te ofrece un
editor de zonas DNS al que puedes añadir registros.

Ejemplos:

+--------+--------------+---------------------+
| Tipo   | Nombre       | Valor               |
+========+==============+=====================+
| A      | @            | IP.DE.TU.SERVIDOR   |
+--------+--------------+---------------------+
| A      | subdominio   | IP.DE.TU.SERVIDOR   |
+--------+--------------+---------------------+

El servidor **puede tardar unos minutos en detectar la nueva configuración de los DNS**, para comprobar que reconoce que el dominio está apuntando al servidor tienes que ir al Panel de Control, a la sección "Dominios" y en el dominio que se haya añadido hacer clic en "Ver DNS".

Se tendrá que ver un tic verde para el registro A como se muestra en la siguiente captura:

.. figure:: img/CrearWeb/dns_ok.png
   :alt: Screenshot

   Configuración DNS correcta.


Activar el servidor web
-----------------------

Una vez la configuración de los DNS sea la correcta y el servidor detecte que el dominio está apuntando al servidor, se podrá activar el servidor web para este domino. Para ello hay que editar el dominio añadido, marcar la casilla "Activar Servidor Web" y darle a "Guardar".

.. figure:: img/CrearWeb/webserver.png
   :alt: Screenshot

   Activación del servidor web.

**Recuerda que tarda unos segundo en activarse**. Es el tiempo necesario para que se cree el *virtual host* de Apache y los certificados de *Let's Encrypt*. Después de unos segundos ya podrás ver en verde "Servidor Web: Activado".

.. figure:: img/CrearWeb/ServidorWeb.png
   :alt: Screenshot

   Servidor web activado.

Una vez el servidor web se haya activado correctamente, el *virtual host* del domino se podrá encontrar en el directorio ``/etc/apache2/ldap-enabled/`` y los certificados de Let's Encrypt en ``/etc/letsencrypt/``.

En el caso de que el dominio ya estuviera apuntando al servidor en el momento de añadirlo al Panel de Control, el servidor web se activará automáticamente.


Certificados con Let's Encrypt
------------------------------

Cuando activas el servidor web para un dominio de la forma que te
describimos anteriormente, automáticamente se genera y se configura un
certificado con `Let's Encrypt <https://letsencrypt.org/>`__ para este
dominio.

No tendrás que hacer nada más para configurar los certificados. Las
conexiones a tu web o aplicación web se harán por https.
