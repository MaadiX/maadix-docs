Panel de Control
================

Alta del servidor
-----------------

Cuando el proceso de creación de una máquina virtual en MaadiX termina, el mismo servidor recién creado envía un correo de notificación a la cuenta de correo electrónico que se ha designado como administradora.

El correo contiene instrucciones sobre cómo acceder al servidor, incluyendo la contraseña de acceso al Panel de Control. Dicha contraseña es generada localmente por el servidor, de modo que nadie más las conoce.

Sin embargo (al menos por el momento), el proceso de envío de esta contraseña mediante correo se efectúa en texto plano, lo que hace absolutamente indispensable que se proceda inmediatamente a cambiarla por cuestiones de seguridad. Es por eso que MaadiX fuerza el cambio de la contraseña a través de un proceso de activación en el momento de la primera autenticación en el Panel de Control.

Activación
----------

La primera vez que se accede al Panel de Control solamente se mostrará
la página de activación. En esta página se tendrá que realizar lo siguiente:

- Cambiar la contraseña de la cuenta de administración del Panel de Control (que se envió por email).
- Crear una contraseña para la cuenta de Superusuarix (root del sistema).
- Crear una contraseña para la cuenta de MySQL (cuenta con permisos de root).

Hasta que no se complete este proceso, las otras secciones del panel de
control no serán accesibles y la cuenta Superusuarix no podrá acceder al
sistema ni por SSH ni por SFTP.

Es sumamente importante que el correo electrónico asociado a la cuenta
de administración del Panel de Control sea válido y que tengas acceso al
mismo, ya que el servidor enviará a esta cuenta todas las
notificaciones, indicaciones e instrucciones para recuperar la clave de
acceso del Panel de Control en caso de que la olvidaras.

.. figure:: img/PanelControl/activation.png
   :alt: Screenshot

   Cambio y asignación de contraseñas en la activación del Panel de Control.



Detalles
--------

La ventana de inicio del Panel de Control muestra información
estadística interna acerca del uso de recursos en el sistema. En ella
encontrarás también información sobre la configuración DNS para el
dominio principal de tu servidor.

.. figure:: img/PanelControl/panel-de-control.png
   :alt: Screenshot

   Detalles del uso de recursos en el sistema.


Recuperar contraseña
--------------------

**Recuerda que el equipo de MaadiX no tiene acceso a estos datos y no
puede hacer este proceso por ti.**

Si olvidas la contraseña de la cuenta de administración del panel de control tendrás que hacer clic en "He olvidado mi contraseña" e introducir el correo electrónico asociado a esta cuenta.

.. figure:: img/PanelControl/recover_pass1.png
   :alt:

   Solicitud de recuperación de contraseña.

Se te enviará un correo electrónico con un enlace para restablecer la contraseña. Una vez designada la nueva contraseña se podrá ir a la página de acceso y acceder con normalidad.

.. figure:: img/PanelControl/recover_pass2.png
   :alt:

   Cambio de contraseña.
