Mumble
======

Mumble es una aplicación muy útil para poder hacer reuniones online. No se trata de una aplicación de vídeo conferencia ya que solo ofrece las opciones de audio y texto. Sin embargo, debido a su gran calidad de sonido y su baja latencia es una herramienta muy indicada para reuniones multitudinarias. Incluye funcionalidades de moderación y turnos de palabras entre otras.
Para su uso es necesario una vez instalado el servidor Mumble, disponer de una aplicación cliente para poder conectar a las salas. 
En esta guía mostramos como proceder para la instalación de ambos.

Instalación
-----------

Desde el Panel de Control, ve al apartado '**Instalar Aplicaciones**',
ahí podrás ver todas las aplicaciones disponibles para instalar, entre
ellas Mumble. Solo tienes que marcar la casilla *'Seleccionar'* e indicar:

- Un **dominio o subdominio** donde lo quieres instalar. Este dominio tiene que estar apuntando a la dirección IP del servidor, para ello tendrás que añadir un registro A en el editor de zonas DNS de tu dominio (operación externa a MaadiX). Recuerda que **este dominio o subdominio NO lo tienes que añadir en la sección 'Dominios'** del panel de control.
- Una **contraseña** para asignarle a la cuenta de administración de Mumble. Guárdala en algún lugar seguro ya que no será enviada por correo por razones de seguridad.


Una vez indicados estos dos parámetros puedes darle al botón de **'Instalar'**.

.. figure:: img/mumble/mumble-install.png
   :alt:

   Instalación de Mumble

Después de unos minutos te llegará un email (a la dirección que tengas configurada para la cuenta administración del Panel de Control) confirmando que la instalación terminó correctamente.

Cambio de dominio
------------------

Si necesitas consultar o cambiar el dominio o subdominio de instalción de Mumble, solo tienes que ir al Panel de Control y en '**Mis aplicaciones**' > '**Mumble**' > '**Configurar**' indicar el nuevo dominio. Este dominio tiene que estar apuntando a la dirección IP del servidor. Para ello tendrás que añadir un registro A en el editor de zonas DNS de tu dominio (operación externa a MaadiX). Recuerda que **este dominio o subdominio no lo tienes que añadir en la sección 'Dominios'**.


Conectar a Mumble
-----------------

Para hacer uso de Mumble necesitas instalar una aplicación cliente. El método de instalación depende del sistema operativo que estés utilizando.  

- Linux: La mayoría de distribuciones incluyen Mumble en sus repositorios, por lo que será suficiente lanzar el comando:

  ``sudo apt-get update && apt-get install mumble``

- Windows o MacOS: descarga la aplicación desde la página oficial haciendo clic en el icono del sistema operativo deseado.
  `Descargar Cliente Mumble <https://www.mumble.com/>`__

La primera vez que abres la aplicación serás guiado en un proceso de configuración de sonido que permite optimizar los niveles de entrada y salida de audio.

Una vez terminado tendrás configurar los parámetros de conexión al servidor Mumble recién instalado.
Para ello haz clic en '**Servidor**' > '**Conectar**' > '**Agregar Nuevo**'.

Hay dos formas de conectar al servidor: con la cuenta de administración (superUser) o con una cuenta sin privilegios.

En ambos casos se mostrará una ventana en la que tendrás que introducir los parámetros.

Para conectar con una cuenta sin privilegios
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. figure:: img/mumble/connect-user.png
   :alt:

   Conectar a servidor mumble
   
Los parámetros son los siguientes:

- Dirección: el dominio/subdominio en el que instalaste mumble. En nuestro ejemplo mumble.example.com
- Puerto: el que hay por defecto (64738)
- Nombre de Usuarix: un nombre de tu elección. Será el nombre que aparecerá privilegios en las salas cuando te conectes
- Etiqueta: Un nombre cualquiera para distinguir esta conexión de otras. 

Para conectar con la cuenta SuperUser
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

En este caso en lugar de un nombre cualquiera tendrás que insertar 'SuperUser' en el campo 'Nombre de Usuarix' .
Aparecerá un campo adicional para la contraseña en el que tendrás que introducir la misma que indicaste durante la instalación. 


.. figure:: img/mumble/connect-superuser.png
   :alt:

   Conectar a servidor mumble como superuser
   

Si accedes como SuperUser tendrás privilegios para poder configurar parámetros avanzados de conexión.


Crear salas
-----------

Por defecto hay una sala creada cuyo nombre es Root. La cuenta SuperUser puede crear salas que aparecerán en el listado de salas disponibles cada vez que alguien conecte al servidor.

Las salas pueden ser públicas o protegidas por contraseña.

La cuenta Supersuser además tiene la capacidad de expulsar usuarix, crear reglas de acceso para las salas y sin fin de otras configuraciones que se escapan de los propósitos de esta mini guía.

Si quieres profundizar en todas las opciones disponibles te recomendamos consultar su documentación oficial
- Guías: https://www.mumble.com/mumble-server-support.php
- Wiki: https://wiki.mumble.info/wiki/Main_Page


